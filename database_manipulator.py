# -*- coding: utf-8 -*-
import sqlite3
import datetime

class data_base():
    def __init__(self):
        self.conn = sqlite3.connect('example.db')
        self.sql_mngr = self.conn.cursor()

        try:
            self.sql_mngr.execute('''CREATE TABLE admins
                         (ID INTEGER PRIMARY KEY, username, password, Created_Date, FullName,
                          Tel, Email, Condition)''')
        except:
            pass
        try:
            self.sql_mngr.execute('''CREATE TABLE players (ID INTEGER PRIMARY KEY, Name, birth_date, Created_Date, sex, Group_text)''')
        except:
            pass

        try:
            self.sql_mngr.execute('''CREATE TABLE pre_games (ID INTEGER PRIMARY KEY, Name, description, Data)''')
        except:
            pass

        try:
            self.sql_mngr.execute('''CREATE TABLE test (ID INTEGER PRIMARY KEY, text)''')
        except:
            pass

        self.conn.commit()

    def login_check(self, user, paswd):

        self.sql_mngr.execute('''SELECT username, password FROM admins WHERE username=?''', (user,))
        item = self.sql_mngr.fetchall()

        if not item:
            return "UserNameError"
        elif paswd == item[0][1]:
            return True
        else:
            return "PasswdError"

    def admin_add(self, username, password, FullName,
                  Tel, Email, Condition):

        crt_date = str(datetime.datetime.now().year) + '-' + str(datetime.datetime.now().month) + '-' + str(
            datetime.datetime.now().day)
        infs = [username, password, crt_date, FullName, Tel, Email, Condition]
        if self.admin_check_exiting(username):
            return "username exist"

        self.sql_mngr.execute('''INSERT INTO admins(username, password, Created_Date, FullName,
                          Tel, Email, Condition) VALUES ( ?, ?, ?, ?, ?, ?, ?)''', infs)
        self.conn.commit()
        return

    def admin_check_exiting(self, username):
        self.sql_mngr.execute('''SELECT * FROM admins WHERE username=?''', (username,))
        q = self.sql_mngr.fetchall()
        if q:
            return True
        else:
            return False

    def admin_remove(self, username):
        if not self.admin_check_exiting(username):
            # print "username not exist"
            return "username not exist"
        self.sql_mngr.execute('''DELETE FROM admins WHERE username= ?;''', (username,))
        self.conn.commit()

    def admin_extract_all(self):

        self.sql_mngr.execute('''SELECT * FROM admins''')
        q = self.sql_mngr.fetchall()
        return q

    def player_add(self, name, birth, group, crt_date, sex):

        if self.player_check_exiting(name, group):
            return

        infs = (name, birth, crt_date, sex, group)
        self.sql_mngr.execute('''INSERT INTO players(Name, birth_date, Created_Date, sex, Group_text) VALUES (?, ?, ?, ?, ?)''', infs)
        self.conn.commit()
        self.sql_mngr.execute('''SELECT MAX(ID) FROM players''')
        ID = self.sql_mngr.fetchall()[0][0]
        self.creat_table_player(ID)
        return ID

    def player_check_exiting(self, name, group):
        self.sql_mngr.execute('''SELECT * FROM players WHERE name=? AND Group_text=?''', (name, group,))
        q = self.sql_mngr.fetchall()
        if q:
            return True
        else:
            return False

    def player_check_exiting_ID(self, ID):
        self.sql_mngr.execute('''SELECT * FROM players WHERE ID=?''', (ID,))
        q = self.sql_mngr.fetchall()
        if q:
            return True
        else:
            return False

    def player_extract_all(self):
        self.sql_mngr.execute('''SELECT * FROM players''')
        q = self.sql_mngr.fetchall()
        return q

    def player_remove_ID(self, ID):
        if not (self.player_check_exiting_ID(ID)):
            return

        self.sql_mngr.execute('''DELETE FROM players WHERE ID=? ''',(ID,))
        try:
            self.sql_mngr.execute('''DROP TABLE player_{}'''.format(ID))
        except sqlite3.OperationalError:
            print("No table")
        self.conn.commit()

    def player_extract_ID(self, ID):
        if not (self.player_check_exiting_ID(ID)):
            return
        self.sql_mngr.execute('''SELECT * FROM players WHERE ID=?''', (ID,))
        q = self.sql_mngr.fetchall()[0]
        dct = {'name': q[1], 'group': q[5], 'age': q[2], 'crt_dte': q[3], 'sex': q[4]}
        return dct

    def game_adding(self, somedata):
        pass

    def game_predesigned_extract_all(self):
        self.sql_mngr.execute('''SELECT * FROM pre_games''')
        q = self.sql_mngr.fetchall()
        return q

    def creat_table_player(self, ID):
        self.sql_mngr.execute(
            '''CREATE TABLE Player_{} (ID INTEGER PRIMARY KEY, Date, test_name, test_res)'''.format(ID))
        self.conn.commit()

    def game_res_add(self, player_ID, Game_name, Game_date, res):
        infs = (Game_name, Game_date, res)
        self.sql_mngr.execute(
            '''INSERT INTO Player_{} (Date, test_name, test_res) VALUES (? ,? ,?)'''.format(player_ID), (infs))
        self.conn.commit()

    def game_exract_played(self, player_ID):
        self.sql_mngr.execute('''SELECT * FROM Player_{}'''.format(player_ID))
        q = self.sql_mngr.fetchall()
        Data = []
        for items in q:
            Data.append({'name': items[1], 'play_dte': items[2], 'ID': str(items[0]), 'res': items[3], 'is_selected': False})
        return Data


    def add_test(self):
        self.sql_mngr.execute('''SELECT MAX(ID) FROM players''')
        mx = self.sql_mngr.fetchall()[0][0]
        print(mx)
        # self.conn.commit()


# ssql= data_base()
#

# ssql.conn.commit()

# q=ssql.player_extract_ID('1')
# print(q)