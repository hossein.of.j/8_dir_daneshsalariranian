from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.core.image import Image as CoreImage
from kivy.graphics import Color, Rectangle
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.uix.video import Video

from arabic_reshaper import reshape
from bidi.algorithm import get_display
from datetime import datetime

import win32api, win32gui
import io

from client import *
from threading import Thread


# imc = [CoreImage(io.BytesIO(open("pic.jpg", "rb").read()), ext="png").texture,
#        CoreImage(io.BytesIO(open("pic1.jpg", "rb").read()), ext="png").texture,
#        CoreImage(io.BytesIO(open("pic2.jpg", "rb").read()), ext="png").texture,
#        CoreImage(io.BytesIO(open("pic3.jpg", "rb").read()), ext="png").texture]

def Makepersian(s: str):
    return get_display(reshape(s))


import win32api

monitors = win32api.EnumDisplayMonitors()
m_cont = len(monitors)
if m_cont == 1:
    print("SecApp ==>", "There is only 1 screen attach secound screen or make dispalys Extended( win_key + P)")
    Window.left = 1200
    # Window.maximize()
    Window.size = (400, 400)
    Window.borderless = True
    Window.top = 500
elif m_cont > 2:
    print("SecApp ==>", "There is more than 2 screen remove one")
else:
    print("SecApp ==>", monitors[1])
    Window.left = monitors[1][2][0]
    Window.maximize()
    Window.borderless = True


class Splash_screen(Screen):

    def on_enter(self, *args):
        Clock.schedule_once(self._finish_enter, 1)

    def _finish_enter(self, dt):
        s = win32gui.FindWindow(None, "Second")
        win32gui.SetWindowPos(s, -1, 1100, 500, 400, 400, 2)

    def __init__(self, *args, **kwargs):
        super(Splash_screen, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        pass


class pre_start(BoxLayout):

    def __init__(self, *args, **kwargs):

        super(pre_start, self).__init__(*args, **kwargs)

        self.time_lb = Label(text="3", font_size=150, color=(0, 0, 0, 1))
        self.add_widget(self.time_lb)

        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        self.size_hint = (1, 1)

    def reset(self):
        self.time_lb.text = "3"

    def Start(self):
        Clock.schedule_once(self.next, 1)

    def next(self, dt):
        c_num = int(self.time_lb.text)
        if c_num == 1:
            self.time_lb.text = "0"
            self.time_lb.texture_update()
            Clock.schedule_once(lambda x: self.parent.stop_pre(), 0.5)
        else:
            self.time_lb.text = str(c_num - 1)
            self.time_lb.texture_update()
            Clock.schedule_once(self.next, dt)


class X_O_Task_screen(Screen):

    def __init__(self, *args, **kwargs):
        super(X_O_Task_screen, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        self.prescreen = pre_start()
        self.add_widget(self.prescreen)
        # self.start()

    def start(self):
        self.prescreen.Start()

    def stop_pre(self):
        self.remove_widget(self.prescreen)
        # self.ids.frame.stop_pre()
        self.manager.socket.send('1')

    def reset(self):
        self.prescreen = pre_start()
        self.add_widget(self.prescreen)


class X_O_Task(GridLayout):
    rec = [None for _ in range(9)]

    def __init__(self, **kwargs):

        super(self.__class__, self).__init__(**kwargs)
        self.cols = 3
        self.spacing = 15
        self.padding = 15
        self.lbs = [Image() for i in range(9)]
        self.im_x = CoreImage(io.BytesIO(open("data\X.png", "rb").read()), ext="png").texture
        self.im_o = CoreImage(io.BytesIO(open("data\O.png", "rb").read()), ext="png").texture
        self.bind(size=lambda x, y: Clock.schedule_once(self._finish_init))
        self.pos_hint = {"center_x": .5, "center_y": .5}
        for i in range(9):
            self.add_widget(self.lbs[i])
            self.lbs[i].source = ""
        # self.lbs[4].texture = self.im_x

        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        win_wid = Window.size[0]
        win_hei = Window.size[1]

        if win_wid < win_hei:
            self.size_hint = (1, None)
            self.height = self.width
        else:
            self.size_hint = (None, 1)
            self.width = self.height

        for i in range(9):
            self.lbs[i].canvas.before.clear()
            with self.lbs[i].canvas.before:
                Color(1, 1, 1, 1)
                self.rec[i] = Rectangle(size=self.lbs[i].size, pos=self.lbs[i].pos)

    def run(self, data):

        for i in range(9):
            self.set_pix(i, int(data[i]))

    def set_pix(self, i, state):
        if state == 0:
            self.lbs[i].texture = None

        elif state == 1:
            self.lbs[i].texture = self.im_x

        elif state == 2:
            self.lbs[i].texture = self.im_o

    def restart(self):
        for i in range(9):
            self.set_pix(i, 0)

        self.parent.parent.reset()
    # def stop_pre(self):


class G_x_Task_screen(Screen):

    def __init__(self, *args, **kwargs):
        super(G_x_Task_screen, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        self.prescreen = pre_start()
        self.add_widget(self.prescreen)
        # self.start()

    def start(self):
        self.prescreen.opacity = 1
        self.prescreen.Start()

    def no_pre(self):
        # Clock.schedule_once(lambda x: self.stop_pre())
        self.prescreen.opacity = 0

    def stop_pre(self):
        self.prescreen.opacity = 0
        self.prescreen.reset()

        # self.ids.frame.stop_pre()
        self.manager.socket.send('1')


    def reset(self):
        self.prescreen = pre_start()
        self.add_widget(self.prescreen)


class G_x_Task(GridLayout):
    rec = [None for _ in range(9)]

    def __init__(self, **kwargs):

        super(self.__class__, self).__init__(**kwargs)
        self.cols = 3
        self.spacing = 15
        self.padding = 15
        self.lbs = [Label(font_size=int(self.width * .8), color=(0, 0, 0, 1)) for i in range(9)]
        self.bind(size=lambda x, y: Clock.schedule_once(self._finish_init))
        self.pos_hint = {"center_x": .5, "center_y": .5}
        for i in range(9):
            self.add_widget(self.lbs[i])
            self.lbs[i].text = ""

        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        win_wid = Window.size[0]
        win_hei = Window.size[1]

        if win_wid < win_hei:
            self.size_hint = (1, None)
            self.height = self.width
        else:
            self.size_hint = (None, 1)
            self.width = self.height

        for i in range(9):
            self.lbs[i].canvas.before.clear()
            with self.lbs[i].canvas.before:
                Color(1, 1, 1, 1)
                self.rec[i] = Rectangle(size=self.lbs[i].size, pos=self.lbs[i].pos)

    def run(self, data):
        if (len(data) == 9) and (data[0] != "["):
            # its mean game is 8-x Task
            for i in range(9):
                self.set_pix(i, int(data[i]))
        else:
            # game is Memory_Task
            data = data[1:-1]
            poss = data.split(",")

            self.clear_lbs()
            for i in range(len(poss) - 1):
                dt = poss[i].split(":")
                self.set_num_pix(int(dt[0]), dt[1])

    def clear_lbs(self):
        for i in range(9):
            self.lbs[i].text = ""

    def set_num_pix(self, idd, st):
        self.lbs[idd].text = st

    def set_pix(self, i, state):
        btn_dict = {'0': 8, '1': 1, "2": 2, '3': 7, '4': 0, '5': 3, '6': 6, '7': 5, '8': 4}
        if state:
            self.lbs[i].text = str(btn_dict[str(i)])
        else:
            self.lbs[i].text = ""

    def restart(self):
        for i in range(9):
            self.lbs[i].text = ""

        self.parent.parent.reset()
    # def stop_pre(self):


class Implicit_Strop_screen(Screen):

    def __init__(self, *args, **kwargs):
        super(Implicit_Strop_screen, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        pass
        # self.start()

    def prescr(self, datas):
        ds = datas
        txt = list()
        cl = list()

        for i in range(3):
            txt1 = ds.split(";")[1 + i].split(":")[0]
            sq = ds.split(";")[1 + i].split(":")[1]
            sq = sq[1:-1]
            sq = sq.split(',')
            col1 = (float(sq[0]), float(sq[1]), float(sq[2]), float(sq[3]))
            txt.append(txt1)

            cl.append(col1)
        # txt = [txt1,"",""]
        # cl = [col1,(0,0,0,0),(0,0,0,0)]

        self.box = BoxLayout()

        self.box.orientation = "vertical"
        self.box.padding = 30
        self.box.spacinig = 20
        Clock.schedule_once(self.back_ground_setter)
        self.f_size = self.width * 0.2
        self.lb = [Label(text=txt[0], font_name="data/arial.ttf", color=cl[0], font_size=self.f_size),
                   Label(text=txt[1], font_name="data/arial.ttf", color=cl[1], font_size=self.f_size),
                   Label(text=txt[2], font_name="data/arial.ttf", color=cl[2], font_size=self.f_size)]
        for i in range(3):
            self.box.add_widget(self.lb[i])
        self.add_widget(self.box)

    def back_ground_setter(self, dt):
        with self.box.canvas.before:
            Color(rgba=(1, 1, 1, 1))
            self.rect = Rectangle(size=self.box.size, pos=self.box.pos)

    def start(self):
        self.prescreen = pre_start()
        self.add_widget(self.prescreen)
        self.remove_widget(self.box)
        self.prescreen.Start()

    def stop_pre(self):
        self.remove_widget(self.prescreen)
        # self.ids.frame.stop_pre()
        self.manager.socket.send('1')

    def reset(self):
        self.prescreen = pre_start()
        self.add_widget(self.prescreen)


class Implisit_strop_Task(GridLayout):
    rec = [None for _ in range(9)]

    def __init__(self, **kwargs):

        super(self.__class__, self).__init__(**kwargs)
        self.cols = 3
        self.spacing = 15
        self.padding = 15
        self.lbs = [Label(font_size=int(self.width * .8), color=(0, 0, 0, 1)) for i in range(9)]
        self.bind(size=lambda x, y: Clock.schedule_once(self._finish_init))
        self.pos_hint = {"center_x": .5, "center_y": .5}
        for i in range(9):
            self.add_widget(self.lbs[i])
            self.lbs[i].text = ""

        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        win_wid = Window.size[0]
        win_hei = Window.size[1]

        if win_wid < win_hei:
            self.size_hint = (1, None)
            self.height = self.width
        else:
            self.size_hint = (None, 1)
            self.width = self.height

        for i in range(9):
            # self.lbs[i].canvas.before.clear()
            with self.lbs[i].canvas.before:
                Color(1, 1, 1, 1)
                self.rec[i] = Rectangle(size=self.lbs[i].size, pos=self.lbs[i].pos)

    def col_set(self, i, col):
        self.lbs[i].canvas.before.clear()
        with self.lbs[i].canvas.before:
            Color(col[0], col[1], col[2], col[3])
            self.rec[i] = Rectangle(size=self.lbs[i].size, pos=self.lbs[i].pos)

    def run(self, data):
        sq = data[1:-1].split(")")

        cols = list()
        for i in range(8):
            qe = sq[i].split("(")[1]
            cs = qe.split(",")
            cols.append((float(cs[0]), float(cs[1]), float(cs[2]), float(cs[3])))

        self.col_set(0, cols[0])
        self.col_set(1, cols[1])
        self.col_set(2, cols[2])
        self.col_set(3, cols[3])
        self.col_set(5, cols[4])
        self.col_set(6, cols[5])
        self.col_set(7, cols[6])
        self.col_set(8, cols[7])

    def restart(self):
        self.parent.parent.reset()


class Designed_Test_Screen(Screen):
    col_codes = {"Black": (0, 0, 0, 1), "White": (1, 1, 1, 1), "Yellow": (1, 1, 0, 1), "Blue": (0, 0, 1, 1),
                 "Magenta": (1, 0, 1, 1), "Green": (0, 1, 0, 1), "Cyan": (0, 1, 1, 1), "Red": (1, 0, 0, 1),
                 "Gray": (.6, .6, .6, 1)}
    dir_pic_addr = {"M": "data/dir/M.png", "N": "data/dir/N.png", "NE": "data/dir/NE.png", "E": "data/dir/E.png",
                    "SE": "data/dir/SE.png",
                    "S": "data/dir/S.png", "SW": "data/dir/SW.png", "W": "data/dir/W.png", "NW": "data/dir/NW.png"}

    def __init__(self, *args, **kwargs):
        super(Designed_Test_Screen, self).__init__(*args, **kwargs)
        self.PSC = BoxLayout(orientation="vertical", padding=[self.height * 0.05], spacing=self.height * .05)
        self.PSC.add_widget(Label())
        self.PSC.add_widget(Label(text=Makepersian("علامت موثر"), size_hint=(.7, .3), pos_hint={"center_x": .5},
                                  color=(0, 0, 0, 1), font_size=self.height * .2))
        self.PSC_LB = Label(size_hint=(.3, None), height=self.width * .5, pos_hint={"center_x": .5}, color=(0, 0, 0, 1),
                            font_size=self.width * .5)
        self.PSC.add_widget(self.PSC_LB)
        self.PSC.add_widget(Label())

        self.Psh_lb = Label(text="", size_hint=(1, 1), color=(0, 0, 0, 1), opacity=0, font_size=self.height * .7)

        self.vid = Video(size_hint=(1,1), pos_hint={'x':0, 'y':0})
        self.vid.volume = 0
        self.vid.opacity = 0

        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        self.add_widget(self.PSC)
        self.add_widget(self.Psh_lb)
        self.add_widget(self.vid)
        with self.PSC.canvas.before:
            Color(rgb=(1, 1, 1))
            self.rec = Rectangle(size=self.size, pos=self.pos)

        self.bind(size=self.rec_up,
                  pos=self.rec_up)

        with self.Psh_lb.canvas.before:
            Color(rgb=(.6, .6, .6))
            self.rec_Psh = Rectangle(size=self.size, pos=self.pos)

    def rec_up(self, *args):
        self.rec.size = self.size
        self.rec.pos = self.pos
        self.rec_Psh.size = self.size
        self.rec_Psh.pos = self.pos

    def set_video_source(self, data):
        print("SecApp ==> ", "vid_source_is", data)
        self.vid.source = data
        # self

    def vid_statuse(self, data):
        if data[0] == "P":
            self.vid.opacity = 1
            self.vid.texture_update()
            self.vid.play = True

        elif data[0] == "S":
            self.vid.opacity = 0
            self.vid.texture_update()
            self.vid.play = False

        elif data[0] == "R":
            self.vid.opacity = 0
            self.vid.texture_update()
            self.vid.play = False

    def prescr(self, data):
        self.PSC.opacity = 1
        if data[0] == "0":
            self.PSC_LB.text = data[1]

        elif data[0] == "1":
            self.PSC_LB.text = ""
            col = data[1:]
            with self.PSC_LB.canvas:
                Color(rgb=self.col_codes[col])
                rec = Rectangle(size=self.PSC_LB.size,
                                pos=self.PSC_LB.pos)
        else:
            dr = data[1:]
            self.PSC_LB.canvas.clear()
            with self.PSC_LB.canvas:
                rec = Rectangle(size=self.PSC_LB.size,
                                pos=self.PSC_LB.pos,
                                source=self.dir_pic_addr[dr])

    def nopscr(self):
        self.PSC.opacity = 0

    def psh_num_counter(self, num):
        self.Psh_lb.text = str(num)
        if num == 0:
            self.Psh_lb.opacity = 0
            self.stop_psh()
        else:
            num -= 1
            Clock.schedule_once(lambda x: self.psh_num_counter(num), 1)

    def stop_psh(self):
        self.Psh_lb.opacity = 0
        self.manager.socket.send('1')

    def pishscr(self, data):

        if data[0] == "1":
            num = int(data[1:])
            self.Psh_lb.opacity = 1
            self.psh_num_counter(num)

        elif data[0] == "2":
            self.Psh_lb.opacity = 1
            if data[1] == "Y":
                self.Psh_lb.text = ""
                with self.Psh_lb.canvas.before:
                    Color(rgb=(1, 1, 0))
                    self.rec_Psh = Rectangle(size=self.size, pos=self.pos)
            elif data[1] == "G":
                with self.Psh_lb.canvas.before:
                    Color(rgb=(0, 1, 0))
                    self.rec_Psh = Rectangle(size=self.size, pos=self.pos)
                Clock.schedule_once(lambda x: self.stop_psh(), 0.5)

            pass

        pass

    def reset(self):
        pass


class Designed_Test_Task(FloatLayout):
    col_codes = {"Black": (0, 0, 0, 1), "White": (1, 1, 1, 1), "Yellow": (1, 1, 0, 1), "Blue": (0, 0, 1, 1),
                 "Magenta": (1, 0, 1, 1), "Green": (0, 1, 0, 1), "Cyan": (0, 1, 1, 1), "Red": (1, 0, 0, 1),
                 "Gray": (.6, .6, .6, 1)}

    dir_pic_addr = {"M": "data/dir/M.png", "N": "data/dir/N.png", "NE": "data/dir/NE.png", "E": "data/dir/E.png",
                    "SE": "data/dir/SE.png",
                    "S": "data/dir/S.png", "SW": "data/dir/SW.png", "W": "data/dir/W.png", "NW": "data/dir/NW.png"}

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)

        btn_def_pos = [{"center_x": .5, "center_y": .5},
                       {"center_x": .5, "center_y": .7},
                       {"center_x": .7, "center_y": .7},
                       {"center_x": .7, "center_y": .5},
                       {"center_x": .7, "center_y": .3},
                       {"center_x": .5, "center_y": .3},
                       {"center_x": .3, "center_y": .3},
                       {"center_x": .3, "center_y": .5},
                       {"center_x": .3, "center_y": .7}]
        self.btns = list()

        for i in range(9):
            bt = Button(text=str(i), font_size=18, size_hint=(.2, .2), pos_hint=btn_def_pos[i])
            bt.background_color = (.6, .6, .6, 1)
            bt.background_normal = ""
            self.btns.append(bt)
            self.add_widget(self.btns[i])

        # self.t_lb = Button(text="test", size_hint=(.2, .2), pos_hint=btn_def_pos[0])

        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        pass

    def run(self, data):
        # print("SecApp ==>", "recived", data)
        btn = data.split(",")
        for i in range(9):
            if btn[i] == "1":
                self.btns[i].opacity = 1.0
            else:
                self.btns[i].opacity = 0
            self.btns[i].texture_update()
        # k =[self.btns[i].opacity for i in range(9)]
        # print("SecApp ==> opa :",k)

    def set_btn_extract(self, data):
        data = data[1:-1]
        b_ps = data.split(")")
        btns_pos = list()
        for i in range(9):
            s = b_ps[i][1:]
            s = s.split(",")
            x = float(s[0].replace('(', ''))
            y = float(s[1])
            btns_pos.append((x, y))

        for i in range(9):
            if btns_pos[i] == (0.0, 0.0):
                self.set_btn_pos(0, i)
            else:
                self.set_btn_pos(1, i, btns_pos[i])

    def set_btn_pos(self, en, i, pos=(0, 0)):
        if en:
            self.btns[i].pos_hint = {"x": pos[0], 'y': pos[1]}
            self.btns[i].opacity = 1
            self.btns[i].disabled = 0
        else:
            self.btns[i].pos_hint = {"x": 1, 'y': 1}
            self.btns[i].opacity = 0
            self.btns[i].disabled = 1

    def set_mode(self, data):
        if data == "0":
            # num
            for i in range(9):
                self.btns[i].background_normal = ""
                self.btns[i].background_color = (.6, .6, .6, 1)

        elif data == "1":
            di = ["M", "N", "NE", "E", "SE", "S", "SW", "W", "NW"]
            # dir
            for i in range(9):
                self.btns[i].background_normal = self.dir_pic_addr[di[i]]
                self.btns[i].background_color = (.1, .6, .6, 1)

        else:
            # col
            di = ["Gray", "Black", "Yellow", "Green", "Cyan", "White", "Blue", "Magenta", "Red"]
            for i in range(9):
                self.btns[i].background_normal = ''
                self.btns[i].background_color = self.col_codes[di[i]]

    def restart(self):
        pass
        # self.parent.parent.reset()


class Main_screen(ScreenManager):

    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        self.close_thread = False
        self.connect_server()
        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        pass
        # self.current = "splash"

    def connect_server(self):
        try:
            self.socket = MySocket()
            self.th = Thread(target=self.chk_dt)
            self.th.start()
        except:

            Clock.schedule_once(lambda x: self.connect_server(), 5)

    def chk_dt(self):
        while True:
            try:
                Data = self.socket.get_data().decode()
            except ConnectionResetError:
                self.th.do_run = False
                print("SecApp ==>", "Connection Lost")
                Clock.schedule_once(lambda x: self.connect_server(), 5)
                break
            print("SecApp ==>", Data, " at ", datetime.now().second , ".",round(datetime.now().microsecond,5))
            if Data[0:5] == "msg>>":
                self.current_screen.ids.frame.run(Data[5:])
                # for i in range(len(Data[5:])):
                #     self.current_screen.ids.frame.set_pix(i,  int(Data[5+i]))
            elif Data[0:5] == "gst>>":
                if Data[6:9] == 'X_O':
                    self.current = 'X_O_Task'
                    # self.socket.send('1')
                elif Data[6:9] == 'g_x':
                    self.current = 'G_x_Task'
                elif Data[6:9] == 'I_S':
                    self.current = "Implicit_strop"
                elif Data[6:9] == "M_T":
                    self.current = 'G_x_Task'
                    Clock.schedule_once(lambda x: self.current_screen.no_pre())
                elif Data[6:9] == "DSN":
                    self.current = 'Designed_Test'

            elif Data[0:5] == "fun>>":

                if Data[6:9] == "pst":
                    self.current_screen.start()
                    # self.socket.send('1')
                elif Data[6:9] == "rst":
                    self.current_screen.ids.frame.restart()
                elif Data[6:9] == "stp":
                    self.current = 'splash'
                elif Data[6:9] == "psc":
                    self.current_screen.prescr(Data[9:])
                elif Data[6:9] == "sbp":
                    self.current_screen.ids.frame.set_btn_extract(Data[9:])
                elif Data[6:9] == "nps":
                    self.current_screen.nopscr()
                elif Data[6:9] == "kms":
                    self.current_screen.ids.frame.set_mode(Data[9:])
                elif Data[6:9] == "psd":
                    self.current_screen.pishscr(Data[9:])
                elif Data[6:9] == "svs":
                    self.current_screen.set_video_source(Data[9:])
                elif Data[6:9] == "svm":
                    try:
                        self.current_screen.vid_statuse(Data[9:])
                    except:
                        pass
            else:
                self.socket.send('0')
                pass


class SecondApp(App):
    def build(self):
        self.sm = Main_screen()

        # self.sm.current = 'splash'

        return self.sm

    def on_config_change(self, config, section, key, value):
        self.bs._finish_init(1)


SecondApp().run()
