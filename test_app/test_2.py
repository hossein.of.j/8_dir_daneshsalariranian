from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.properties import StringProperty
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button

import socket
import subprocess
import random
from datetime import datetime


subprocess.Popen([r"python", r"C:\Users\hossein\Desktop\test_app\main.py"])


situaiions= [["110000000", '2'],["101000000", '1'],["100010000", '8'],
             ["100100000", '6'],["011000000", '0'],["010010000", '7'],
             ["001010000", '6'],["001001000", '8'],["000110000", '5'],
             ["000100100", '0'],["000011000", '3'],["000010100", '2'],
             ["000010010", '1'],["000010001", '0'],["000001001", '2'],
             ["000000110", '8'],["000000011", '6']]


def list_to_str(ls):
    string = ""
    for i in range(len(ls)):
        string = string + str(ls[i])

    return string


class X_O_Task():
    situaiions = [["110000000", '2'], ["101000000", '1'], ["100010000", '8'],
                  ["100100000", '6'], ["011000000", '0'], ["010010000", '7'],
                  ["001010000", '6'], ["001001000", '8'], ["000110000", '5'],
                  ["000100100", '0'], ["000011000", '3'], ["000010100", '2'],
                  ["000010010", '1'], ["000010001", '0'], ["000001001", '2'],
                  ["000000110", '8'], ["000000011", '6']]

    run_id =0
    def __init__(self):
        serversocket = socket.socket()
        host = 'localhost'
        port = 22334

        serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        serversocket.bind(('', port))

        serversocket.listen(1)
        self.clientsocket, addr = serversocket.accept()

        msg = "gst>> X_O"
        self.clientsocket.send(msg.encode())
        res = self.clientsocket.recv(256)
        if res.decode() != '1': print("gst not sent")
    #
    def rand_select(self):
        self.run_id = int((random.random()*datetime.now().microsecond)%9)
        self._send()

    def clear_buffer(self):
        try:
            while self.clientsocket.recv(1024): pass
        except:
            pass


    def start_pre(self):
        print("start start")
        self.clear_buffer()

        msg = "fun>> pst"

        self.clientsocket.send(msg.encode())
        print("data sent")
        # while True:
        #     try:
        #
        #         res = self.clientsocket.recv(256)
        #         if not res:
        #             break
        #     except:
        #         pass

        res = self.clientsocket.recv(256)
        if res.decode() != '1': print("fun not sent")
        return True

    def _send(self):
        msg = "msg>>" + self.situaiions[self.run_id][0]
        self.clientsocket.send(msg.encode())
        res = self.clientsocket.recv(256)
        if res.decode() != '1': print("msg not sent")

    def check_ans(self, ans):
        if self.situaiions[self.run_id][1] == str(ans):
            return True
        else:
            False




class TestApp(App):
    ite = StringProperty("0")
    itee = 0
    state = [0 for i in range(9)]

    def build(self):
        self.box = GridLayout(cols=3, rows=3, spacing=15)
        self.btns = [Button(text=str(i)) for i in range(9)]
        self.btns[0].bind(on_release=lambda x: self.prs(0))
        self.btns[1].bind(on_release=lambda x: self.prs(1))
        self.btns[2].bind(on_release=lambda x: self.prs(2))
        self.btns[3].bind(on_release=lambda x: self.prs(3))
        self.btns[4].bind(on_release=lambda x: self.prs(4))
        self.btns[5].bind(on_release=lambda x: self.prs(5))
        self.btns[6].bind(on_release=lambda x: self.prs(6))
        self.btns[7].bind(on_release=lambda x: self.prs(7))
        self.btns[8].bind(on_release=lambda x: self.prs(8))

        for i in range(9):
            self.box.add_widget(self.btns[i])

        self.game= X_O_Task()
        # self.game.rand_select()
        return self.box

    def prs(self, i):
        if i == 0:
            k=self.game.start_pre()
            if k:print("Finished")

        if self.game.check_ans(i):
            print("True")
            self.game.rand_select()
        else: print("False")

TestApp().run()
