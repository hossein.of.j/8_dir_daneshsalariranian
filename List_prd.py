# -*- coding: utf-8 -*-
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.listview import SelectableView, ListItemReprMixin
from kivy.uix.button import Button
from kivy.properties import StringProperty, NumericProperty, BooleanProperty


class CIButton(ListItemReprMixin, SelectableView, Button):

    def select(self, *args):
        #if isinstance(self.parent, CItem):
        self.parent.select_from_child(self, *args)

    def deselect(self, *args):
        #if isinstance(self.parent, CItem):
        self.parent.deselect_from_child(self, *args)


class CItem(SelectableView, BoxLayout):
    name = StringProperty()
    age = StringProperty()
    group = StringProperty()
    sex = StringProperty()
    crt_date = StringProperty()
    ID = StringProperty("ID-")
    is_selected = BooleanProperty(0)

    def select(self, *args):
        self.is_selected = True

    def deselect(self, *args):
        self.is_selected = False

    def select_from_child(self, child, *args):
        self.select()

    def deselect_from_child(self, child, *args):
        self.deselect()


class gamehistoryItem(SelectableView, BoxLayout):
    name = StringProperty()
    play_dte = StringProperty()
    res = StringProperty()
    ID = StringProperty("ID-")
    is_selected = BooleanProperty(0)

    def select(self, *args):
        # print(self.ID)
        self.is_selected = True

    def deselect(self, *args):
        self.is_selected = False

    def select_from_child(self, child, *args):
        self.select()

    def deselect_from_child(self, child, *args):
        self.deselect()


class adminItem(SelectableView, BoxLayout):
    name = StringProperty()
    condition = StringProperty()
    email = StringProperty()
    tel = StringProperty()
    username = StringProperty()
    ID = StringProperty("ID-")
    is_selected = BooleanProperty(0)

    def select(self, *args):
        print(self.ID)
        self.is_selected = True

    def deselect(self, *args):
        self.is_selected = False

    def select_from_child(self, child, *args):
        self.select()

    def deselect_from_child(self, child, *args):
        self.deselect()

