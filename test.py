import kivy
kivy.require("1.9.1")
from kivy.app import App
from kivy.lang.builder import Builder
from kivy.uix.relativelayout import RelativeLayout
from kivy.core.audio import SoundLoader
from kivy.uix.video import Video
from kivy.uix.dropdown import DropDown
from kivy.uix.boxlayout import BoxLayout

Builder.load_string('''
<DropBox>:
    Label:
        size_hint_x: .8
        
<main>:
    Button:
        text: "loadFileChooser"
        size_hint: .1,.1
        pos_hint: {"center_x":.5,"center_y":.5}
        on_release: root.do()

''')

class DropBox(BoxLayout):
    pass

class main(RelativeLayout):

    def do(self):
        # self.vid = Video(source="ss.mp4", size_hint=(.9,.9),
        #                  pos_hint={"center_x":.5, "center_y":.5})
        # self.add_widget(self.vid)
        # self.vid.volume = 0
        # self.vid.play = True

        self.snd = SoundLoader.load("ss.mp3")
        self.snd.play()
        # print(self.snd)
        pass

class TesterApp(App):
    def build(self):
        return main()


TesterApp().run()
