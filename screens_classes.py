# -*- coding: utf-8 -*-
from kivy.uix.screenmanager import ScreenManager, Screen, SlideTransition
from kivy.uix.popup import Popup
from kivy.adapters.listadapter import ListAdapter
from kivy.clock import Clock
from kivy.uix.label import Label
from kivy.uix.dropdown import DropDown
from kivy.uix.spinner import Spinner
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.relativelayout import RelativeLayout
from kivy.properties import StringProperty
from bidi.algorithm import get_display
import arabic_reshaper
from List_prd import *
from kivy.app import App
from kivy.core.window import Window
from kivy.graphics import Color, Rectangle
from kivy.garden.FileBrowser import FileBrowser
from kivy.core.audio import SoundLoader
from kivy.config import Config
import socket
import datetime
import subprocess
import game_classes

from database_manipulator import data_base as sql
import win32gui

import psutil
from functools import partial
import toxlsx as XLSX
import serial_emulator as SER
import winsound


def warn():
    frequency = 890  # Set Frequency To 2500 Hertz
    duration = 500  # Set Duration To 1000 ms == 1 second
    winsound.Beep(frequency, duration)


def checkIfProcessRunning(processName):
    '''
    Check if there is any running process that contains the given name processName.
    '''
    # Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False;


class GDropBox(DropDown):
    def __init__(self, *args, **kwargs):
        super(GDropBox, self).__init__(*args, **kwargs)
        self.container.spacing = 1
        self.container.padding = 3, 3, 3, 3

    def addcnt(self, btns):
        for i in btns:
            btn = Dbtn(text=i, width=100, height=20, font_size=16)
            btn.bind(on_release=lambda btn: self.select(btn.text))
            self.add_widget(btn)


class WPopup(Popup):
    def __init__(self, *args, **kwargs):
        super(WPopup, self).__init__(*args, **kwargs)
        self.background = ""
        # self.background_color=[.9,.9,.9,.7]
        self.title_color = [0, 0, 0, 1]
        self.title_align = "right"

    def on_open(self):
        warn()


class Label_f(Label):
    def __init__(self, *args, **kwargs):
        super(Label_f, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init, -1)

    def _finish_init(self, dt):
        self.text = Make_persian(self.text)


class Button_f(Button):
    def __init__(self, *args, **kwargs):
        super(Button_f, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init, -1)

    def _finish_init(self, dt):
        self.text = Make_persian(self.text)


class Sc_mngr(ScreenManager):
    selected_games = list()

    pass


class ToolTip(Label_f):
    pass


class Cbtn(Button_f):
    tip = ""

    def __init__(self, **kwargs):
        Window.bind(mouse_pos=self.on_mouse_pos)
        super(Button, self).__init__(**kwargs)
        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        self.tooltip = ToolTip(text=self.tip)

    def on_mouse_pos(self, *args):
        if not self.get_root_window():
            return
        pos = args[1]
        try:
            self.tooltip.pos = (
                pos[0] + 20, pos[1] - 40)  # (pos[0] - (Window.size[0] / 2) + 70, pos[1] - (Window.size[1] / 2) - 30)
        except AttributeError:
            return
        Clock.unschedule(self.display_tooltip)  # cancel scheduled event since I moved the cursor
        self.close_tooltip()  # close if it's opened
        if self.collide_point(*self.to_widget(*pos)):
            Clock.schedule_once(self.display_tooltip, .5)

    def close_tooltip(self, *args):
        Window.remove_widget(self.tooltip)

    def display_tooltip(self, *args):
        Window.add_widget(self.tooltip)
        Clock.schedule_once(self.close_tooltip, 2)


class Dbtn(Button):
    pass


class Day(DropDown):

    def __init__(self, *args, **kwargs):
        super(Day, self).__init__(*args, **kwargs)
        self.container.spacing = 1
        self.container.padding = 3, 3, 3, 3
        for i in range(30):
            btn = Dbtn(text=str(i + 1), width=34, height=20, font_size=16)
            btn.bind(on_release=lambda btn: self.select(btn.text))
            self.add_widget(btn)


class Month(DropDown):

    def __init__(self, *args, **kwargs):
        super(Month, self).__init__(*args, **kwargs)
        self.container.spacing = 1
        self.container.padding = 3, 3, 3, 3
        for i in range(12):
            btn = Dbtn(text=str(i + 1), width=44, height=20, font_size=16)
            btn.bind(on_release=lambda btn: self.select(btn.text))
            self.add_widget(btn)


class Year(DropDown):

    def __init__(self, *args, **kwargs):
        super(Year, self).__init__(*args, **kwargs)
        self.container.spacing = 3
        self.container.padding = 3, 3, 3, 3
        yrs = [i for i in range(1390, 1360, -1)]
        for index in range(len(yrs)):
            btn = Dbtn(text=str(yrs[index]), width=44, height=20, font_size=14, bold=False)
            btn.bind(on_release=lambda bton: self.select(bton.text))
            self.add_widget(btn)


# noinspection PyArgumentList
class Designing(BoxLayout):
    col_code = {"Black": (0, 0, 0, 1), "White": (1, 1, 1, 1), "Yellow": (1, 1, 0, 1), "Blue": (0, 0, 1, 1),
                "Magenta": (1, 0, 1, 1), "Green": (0, 1, 0, 1), "Cyan": (0, 1, 1, 1), "Red": (1, 0, 0, 1),
                "Gray": (.6, .6, .6, 1)}
    dir_addr = {"M": "data/dir/M.png", "N": "data/dir/N.png", "NE": "data/dir/NE.png", "E": "data/dir/E.png",
                "SE": "data/dir/SE.png",
                "S": "data/dir/S.png", "SW": "data/dir/SW.png", "W": "data/dir/W.png", "NW": "data/dir/NW.png"}

    def __init__(self, caller, *args, **kwargs):
        self.caller = caller
        super(Designing, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init)
        self.d_m_addr = None

    def _finish_init(self, dt):
        self.ids["a_type"].text = Make_persian("")
        self.ids["a_type"].values = (Make_persian("ساده"), Make_persian("انتخابی"), Make_persian("افتراقی"))
        self.ids["a_type"].bind(text=self.a_type_selection)
        self.ids["warning_type"].text = ""
        self.ids["warning_type"].values = (
            Make_persian("پخش صدای بوق"), Make_persian("ترتیب اعداد"), Make_persian("رنگ صفحه زرد و سبز"))
        self.ids["m_presentation"].text = ""
        self.ids["m_presentation"].values = (Make_persian("دیداری"), Make_persian("شنیداری"))
        self.ids["m_type"].text = ""
        self.ids["m_type"].values = (Make_persian("جهت"), Make_persian("عدد"), Make_persian("رنگ"))
        self.ids["m_type"].bind(text=self.m_type_change)
        self.ids["double_m_chk"].bind(active=self.double_m)
        self.load_last()

    def m_type_change(self, *args):
        if self.ids["m_type"].text == Make_persian("جهت"):
            self.set_btn_markers(mod="dir")
        elif self.ids["m_type"].text == Make_persian("رنگ"):
            self.set_btn_markers(mod="color")
        else:
            self.set_btn_markers()

    def double_m(self, ins, active):
        if active:
            if self.ids["m_presentation"].text == "ﯼﺭﺍﺪﯾﺩ":
                self.ids["film_load"].disabled = True
                self.ids["audio_load"].disabled = False
            elif self.ids["m_presentation"].text == "ﯼﺭﺍﺪﯿﻨﺷ":
                self.ids["film_load"].disabled = False
                self.ids["audio_load"].disabled = True
            else:
                ins.active = False
        else:
            self.ids["film_load"].disabled = True
            self.ids["audio_load"].disabled = True

    def a_type_selection(self, ins, text):

        self.a_type = Make_persian(text)

        if self.a_type == "ﺳﺎﺩﻩ":
            self.ids['moharek_cnt'].val = 1
            self.ids['moharek_cnt'].rdisabled = True
            self.ids["m_p_time"].disabled = True
            self.ids["m_p_time"].opacity = 0
            self.ids["m_p_time_text"].opacity = 0
            self.ids["m_m_time"].disabled = True
            self.ids["m_m_time"].opacity = 0
            self.ids["m_m_time_text"].opacity = 0

            self.ids["nasazegary_chk"].disabled = True


        elif self.a_type == "ﺍﻧﺘﺨﺎﺑﯽ":
            self.ids['moharek_cnt'].val = 3
            self.ids['moharek_cnt'].rdisabled = False
            self.ids['moharek_cnt'].bind(val=self.pskh_update)
            self.ids["m_p_time"].disabled = False
            self.ids["m_p_time"].opacity = 1
            self.ids["m_p_time_text"].opacity = 1
            self.ids["m_m_time"].disabled = False
            self.ids["m_m_time"].opacity = 1
            self.ids["m_m_time_text"].opacity = 1
            self.ids["nasazegary_chk"].disabled = False

        elif self.a_type == "ﺍﻓﺘﺮﺍﻗﯽ":
            self.ids['moharek_cnt'].rdisabled = False
            self.ids['moharek_cnt'].unbind(val=self.pskh_update)
            self.ids["m_p_time"].disabled = False
            self.ids["m_p_time"].opacity = 1
            self.ids["m_p_time_text"].opacity = 1
            self.ids["m_m_time"].disabled = False
            self.ids["m_m_time"].opacity = 1
            self.ids["m_m_time_text"].opacity = 1
            self.ids["nasazegary_chk"].disabled = False

        else:
            raise Exception("A_Type_Selection Error")

    def pskh_update(self, *args):
        # self.ids["pasokh_cnt"].text = str(self.ids['moharek_cnt'].val)
        pass

    def Go(self):
        res = self.get_settings
        if res:
            self.save_last()
            self.caller.go_designed(self.setting_dict)
            self.caller.des_pop.dismiss()

    def cncl(self):
        self.caller.des_pop.dismiss()

    # def execl(self):
    #    pass
    def save_last(self):
        lst = [self.setting_dict['az_type'], self.setting_dict["m_cnt"], self.setting_dict["warner"],
               self.setting_dict["pish_time"], self.setting_dict["m_present"], self.setting_dict["m_type"],
               self.setting_dict["m_p_time"], self.setting_dict["m_m_time"], self.setting_dict["dbl_m"],
               self.setting_dict["dbl_m_addr"], self.setting_dict["nasazegary"], self.setting_dict["btn_pos"]]
        string = ""
        for items in lst:
            string = string + "::" + str(items)

        f = open("last_des_data.in", 'w')
        f.write(string)
        f.close()

    def load_last(self):
        try:
            f = open("last_des_data.in", 'r')
        except FileNotFoundError:
            return
        s = f.readline()
        f.close()
        settings_s = s.split("::")
        if settings_s[1] == '0':
            self.ids["a_type"].text = Make_persian("ساده")
        elif settings_s[1] == '1':
            self.ids["a_type"].text = Make_persian("انتخابی")
        elif settings_s[1] == '2':
            self.ids["a_type"].text = Make_persian("افتراقی")

        self.ids["moharek_cnt"].val = str(int(settings_s[2]))

        if settings_s[3] == '0':
            self.ids["warning_type"].text = Make_persian("پخش صدای بوق")
        elif settings_s[3] == '1':
            self.ids["warning_type"].text = Make_persian("ترتیب اعداد")
        elif settings_s[3] == '2':
            self.ids["warning_type"].text = Make_persian("رنگ صفحه زرد و سبز")

        self.ids["pish_dore_time"].text = settings_s[4]

        if bool(int(settings_s[5])):
            self.ids["m_presentation"].text = Make_persian("شنیداری")
        else:
            self.ids["m_presentation"].text = Make_persian("دیداری")

        if settings_s[6] == "0":
            self.ids["m_type"].text = Make_persian("عدد")
        elif settings_s[6] == "1":
            self.ids["m_type"].text = Make_persian("جهت")
        else:
            self.ids["m_type"].text = Make_persian("رنگ")

        self.ids["m_p_time"].text = settings_s[7]
        self.ids["m_m_time"].text = settings_s[8]

    @property
    def get_settings(self):
        a_type = self.ids["a_type"].text
        w_type = self.ids["warning_type"].text
        m_p_type = self.ids["m_presentation"].text
        m_type = self.ids["m_type"].text

        if a_type == Make_persian("ساده"):
            self.azmoon_type = 0
            self.pishdore_time = float(self.ids["pish_dore_time"].text)
            self.moharek_cnt = 1
            self.m_p_time = 0
            self.nasazegary = 0
            self.m_m_time = 0

        elif a_type == Make_persian("انتخابی"):
            self.azmoon_type = 1
            self.moharek_cnt = int(self.ids["moharek_cnt"].val)
            self.pishdore_time = float(self.ids["pish_dore_time"].text)
            self.m_p_time = float(self.ids["m_p_time"].text)
            self.m_m_time = float(self.ids["m_m_time"].text)
            self.nasazegary = self.ids["nasazegary_chk"].active
            self.m_m_time = 0

        elif a_type == Make_persian("افتراقی"):
            self.azmoon_type = 2
            self.moharek_cnt = int(self.ids["moharek_cnt"].val)
            self.pishdore_time = float(self.ids["pish_dore_time"].text)
            self.m_p_time = float(self.ids["m_p_time"].text)
            self.m_m_time = float(self.ids["m_m_time"].text)
            self.nasazegary = self.ids["nasazegary_chk"].active

        else:
            self.Show_error("نوع آزمون انتخاب نشده است")
            return False

        if w_type == Make_persian("پخش صدای بوق"):
            self.warner_type = 0
        elif w_type == Make_persian("ترتیب اعداد"):
            self.warner_type = 1
        elif w_type == Make_persian("رنگ صفحه زرد و سبز"):
            self.warner_type = 2
        else:
            self.Show_error("نوع آگاهی دهنده انتخاب نشده است")
            return False

        if m_p_type == Make_persian("دیداری"):
            self.m_present = 0
        elif m_p_type == Make_persian("شنیداری"):
            self.m_present = 1
        else:
            self.Show_error("نوع ارائه محرک انتخاب نشده است")
            return False

        if m_type == Make_persian("جهت"):
            self.m_type = 1
        elif m_type == Make_persian("عدد"):
            self.m_type = 0
        elif m_type == Make_persian("رنگ"):
            self.m_type = 2
        else:
            self.Show_error("نوع محرک انتخاب نشده است")
            return False

        if self.ids["double_m_chk"].active:
            self.d_m = 1
            if not self.d_m_addr:
                self.Show_error("فایل محرک دوگانه انتخاب نشده است")
        else:
            self.d_m = 0
            self.d_m_addr = None

        self.btn_pos = self.btn_pos_checker()
        self.ids["chideman"].export_to_png(r"data\\btns.png")
        self.setting_dict = {"az_type": self.azmoon_type, "m_cnt": self.moharek_cnt, "warner": self.warner_type,
                             "pish_time": self.pishdore_time,
                             "m_present": self.m_present, "m_type": self.m_type, "m_p_time": self.m_p_time,
                             "m_m_time": self.m_m_time,
                             "dbl_m": self.d_m, "dbl_m_addr": self.d_m_addr, "nasazegary": self.nasazegary,
                             "btn_pos": self.btn_pos}
        nas = {0: 0, 1: 5, 2: 6, 3: 7, 4: 8, 5: 1, 6: 2, 7: 3, 8: 4}
        if self.nasazegary:
            for i in range(9):
                if self.btn_pos[i] != (0, 0):
                    if self.btn_pos[nas[i]] == (0, 0):
                        msg = "یکی از پاسخ های ناسازگار در صفحه نمی باشد"
                        self.Show_error(msg)
                        return False

        return self.setting_dict

    def btn_pos_checker(self):
        work_area = (self.ids["workarea"].pos[0], self.ids["workarea"].pos[1], self.ids["workarea"].width,
                     self.ids["workarea"].height)
        btns = [(0, 0) for _ in range(9)]

        for i in range(9):
            bp = self.ids["cmd_" + str(i)].pos
            if (work_area[0] < bp[0] < (work_area[0] + work_area[2])) and (
                    work_area[1] < bp[1] < (work_area[1] + work_area[3])):
                pos_rel_x = (bp[0] - work_area[0]) / work_area[2]
                pos_rel_y = (bp[1] - work_area[1]) / work_area[3]
                btns[i] = (round(pos_rel_x, 4), round(pos_rel_y, 4))
            else:
                btns[i] = (0, 0)

        return btns

    def loader(self, mode=0):
        if mode:
            # it"s audio
            format_acceptable = ["*.wav", "*.mp3"]
        else:
            # it's video
            format_acceptable = ["*.mp4", "*.mkv", "*.avi"]

        FB = FileBrowser()
        FB.filters = format_acceptable

        FB.cancel_string = "Close"
        FB.select_string = "Load"

        FB.bind(on_success=self._FB_submitt,
                on_canceled=self._FB_close)

        self.FB_pop = Popup(title="File Selection", content=FB, auto_dismiss=False,
                            size_hint=(.6, .9), pos_hint={"center_x": .5, "center_y": .5})
        self.FB_pop.open()

    def _FB_close(self, instance):
        self.FB_pop.dismiss()

    def _FB_submitt(self, instance):
        self.d_m_addr = instance.selection[0]
        if self.d_m_addr:
            self.FB_pop.dismiss()
        else:
            pass

    def Show_error(self, prompt):
        pp = Popup(title=Make_persian("خطا"), title_font="data/BBadr.ttf", title_size=20, title_align="right",
                   size_hint=(.4, .2),
                   content=Label_f(text=prompt, font_size=14))
        pp.open()

    def set_def_pos(self):
        w_id = self.ids.workarea
        p = w_id.pos
        w = w_id.width
        h = w_id.height

        poss = [(p[0] + w / 2 - 35, p[1] + h / 2 - 35),
                (p[0] + w / 2 - 35, p[1] + h / 2 + 35),
                (p[0] + w / 2 + 35, p[1] + h / 2 + 35),
                (p[0] + w / 2 + 35, p[1] + h / 2 - 35),
                (p[0] + w / 2 + 35, p[1] + h / 2 - 105),
                (p[0] + w / 2 - 35, p[1] + h / 2 - 105),
                (p[0] + w / 2 - 105, p[1] + h / 2 - 105),
                (p[0] + w / 2 - 105, p[1] + h / 2 - 35),
                (p[0] + w / 2 - 105, p[1] + h / 2 + 35)]

        for i in range(9):
            self.ids["cmd_" + str(i)].pos = poss[i]

    def set_pos(self, w, pos):
        self.en_btn.pos = (pos[0] - (self.en_btn.width / 2), pos[1] - (self.en_btn.height / 2))

    def positioning(self, instance):
        self.en_btn = instance
        if instance.state == "down":
            Window.bind(mouse_pos=self.set_pos)
        else:
            Window.unbind(mouse_pos=self.set_pos)

    def set_btn_markers(self, mod="num"):
        if mod == "num":
            for i in range(9):
                self.ids["cmd_" + str(i)].background_color = (.6, .6, .6, 1)
                self.ids["cmd_" + str(i)].background_normal = ""
        elif mod == "color":
            for i in range(9):
                self.ids["cmd_" + str(i)].background_normal = ""

            self.ids["cmd_0"].background_color = self.col_code["Gray"]
            self.ids["cmd_1"].background_color = self.col_code["Black"]
            self.ids["cmd_2"].background_color = self.col_code["Yellow"]
            self.ids["cmd_3"].background_color = self.col_code["Green"]
            self.ids["cmd_4"].background_color = self.col_code["Cyan"]
            self.ids["cmd_5"].background_color = self.col_code["White"]
            self.ids["cmd_6"].background_color = self.col_code["Blue"]
            self.ids["cmd_7"].background_color = self.col_code["Magenta"]
            self.ids["cmd_8"].background_color = self.col_code["Red"]
        elif mod == "dir":
            self.ids["cmd_0"].background_normal = self.dir_addr["M"]
            self.ids["cmd_1"].background_normal = self.dir_addr["N"]
            self.ids["cmd_2"].background_normal = self.dir_addr["NE"]
            self.ids["cmd_3"].background_normal = self.dir_addr["E"]
            self.ids["cmd_4"].background_normal = self.dir_addr["SE"]
            self.ids["cmd_5"].background_normal = self.dir_addr["S"]
            self.ids["cmd_6"].background_normal = self.dir_addr["SW"]
            self.ids["cmd_7"].background_normal = self.dir_addr["W"]
            self.ids["cmd_8"].background_normal = self.dir_addr["NW"]


class Documenter(Screen):

    def __init__(self, *args, **kwargs):
        self.sql = sql()
        self.klist = self.Data_Extractor()

        self.catalogLA = ListAdapter(data=self.klist, args_converter=self.args_converter,
                                     cls=CItem, selection_mode='multiple',
                                     allow_empty_selection=True, propagate_selection_to_data=True)
        super(self.__class__, self).__init__(*args, **kwargs)

        self.day_drop = Day()
        self.month_drop = Month()
        # self.year_drop = Year()

        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        self.app = App.get_running_app()
        self.ids['Listing'].adapter = self.catalogLA
        self.catalogLA.data = sorted(self.catalogLA.data, key=lambda item: item['crt_date'], reverse=True)

        self.ids.doc_age_month.text = Make_persian("ماه")
        self.ids.doc_age_day.text = Make_persian("روز")
        self.ids.doc_age_year.text_hint = Make_persian("سال")
        self.ids.doc_age_day.bind(on_release=self.day_drop.open)
        self.ids.doc_age_month.bind(on_release=self.month_drop.open)
        # self.ids.doc_age_year.bind(on_release=self.year_drop.open)
        self.day_drop.bind(on_select=lambda instance, x: setattr(self.ids.doc_age_day, 'text', x))
        self.month_drop.bind(on_select=lambda instance, x: setattr(self.ids.doc_age_month, 'text', x))
        # self.year_drop.bind(on_select=lambda instance, x: setattr(self.ids.doc_age_year, 'text', x))

    def Data_Extractor(self):
        Data = []
        q = self.sql.player_extract_all()
        for item in q:
            var = {'name': item[1], 'age': item[2], 'crt_date': item[3], 'sex': item[4], 'group': item[5],
                   'is_selected': False, 'ID': str(item[0])}
            Data.append(var)
        return Data

    def adding(self):

        name = Make_persian(self.ids.doc_family_text.text)

        age = str(self.ids.doc_age_year.text) + "-" + str(self.ids.doc_age_month.text) + "-" + str(
            self.ids.doc_age_day.text)
        group = Make_persian(self.ids.doc_group_text.text)
        sex = self.ids.doc_sex_m_val.active

        if name == 'ﻧﺎﻡ ﻭ ﻧﺎﻡ ﺧﺎﻧﻮﺍﺩﮔﯽ' or name == "":
            WPopup(title=Make_persian('خطا'), content=Label_f(text='نام را وارد کنید!', color=[0, 0, 0, 1]),
                   size_hint=(None, None), size=(300, 100)).open()
            return
        try:
            int(self.ids.doc_age_month.text)
        except:
            Popup(title=Make_persian(''), content=Label_f(text='سن را درست وارد کنید!'),
                  size_hint=(None, None), size=(300, 200)).open()
            return

        if group == "ﮔﺮﻭﻩ" or group == "":
            group = "  "

        if sex:
            sex = 'مرد'
        else:
            sex = 'زن'
        now = datetime.datetime.now()
        crt_dat = str(now.year) + "-" + str(now.month) + "-" + str(now.day)
        idc = self.sql.player_add(name, age, group, crt_dat, sex)
        item = [
            {'name': name, 'age': age, 'group': group, 'sex': sex, 'crt_date': crt_dat, 'is_selected': True,
             'ID': str(idc)}]
        self.catalogLA.data = item + self.catalogLA.data

    def r_remove(self, dt):
        self.rpop.dismiss()
        selecteds = self.catalogLA.selection
        for item in selecteds:
            try:
                ID = item.parent.ID
            except:
                ID = item.ID
            self.sql.player_remove_ID(ID)

        self.catalogLA.data = self.Data_Extractor()

    def remove(self):
        selecteds = self.catalogLA.selection
        cnt = BoxLayout(orientation="vertical", padding=[40, 40, 40, 40], spacing=30)
        lb = Label_f(text='آیا مطمئنید؟', size_hint_y=0.4, color=[0, 0, 0, 1])
        btn = Button_f(text="بله", size_hint_x=0.3, size_hint_y=0.6, pos_hint={'center_x': .5})
        btn.bind(on_release=self.r_remove)
        cnt.add_widget(lb)
        cnt.add_widget(btn)
        if selecteds:
            self.rpop = WPopup(title=Make_persian('خطا'), title_font="data/BBadr.ttf", title_size=24, content=cnt,
                               size_hint=(None, None), size=(400, 230))

            self.rpop.open()

    def args_converter(self, row_index, an_obj):
        return {'name': Make_persian(an_obj['name']),
                'age': an_obj['age'],
                'ID': an_obj['ID'],
                'group': Make_persian(an_obj['group']),
                'crt_date': Make_persian(an_obj['crt_date']),
                'sex': Make_persian(an_obj['sex'])}

    def sorter(self, mod, state='normal'):
        if state == "normal":
            rv = False
        else:
            rv = True
        if mod == 'name':
            self.catalogLA.data = sorted(self.catalogLA.data, key=lambda item: item['name'], reverse=rv)
        elif mod == 'age':
            self.catalogLA.data = sorted(self.catalogLA.data, key=lambda item: item['age'], reverse=rv)
        elif mod == 'group':
            self.catalogLA.data = sorted(self.catalogLA.data, key=lambda item: item['group'], reverse=rv)
        elif mod == 'sex':
            self.catalogLA.data = sorted(self.catalogLA.data, key=lambda item: item['sex'], reverse=rv)
        elif mod == 'crt_date':
            self.catalogLA.data = sorted(self.catalogLA.data, key=lambda item: item['crt_date'], reverse=not rv)
        else:
            pass
        return

    def start_matches(self):
        # print("----------------")
        selecteds = self.catalogLA.selection
        if not selecteds:
            WPopup(title=Make_persian("خطا"), title_font="data/BBadr.ttf", title_size=24,
                   content=Label_f(text="موردی انتخاب نشده است!!", color=[0, 0, 0, 1]), size_hint=(None, None),
                   size=(400, 230)).open()
            return
        self.app.Base.player_list = []
        # self.app.Base.ids.tpp_2.Make_on()
        self.app.Base.ids.tpp_1.Make_off()

        for item in selecteds:
            try:
                ID = item.parent.ID
            except:
                ID = item.ID
            self.app.Base.player_list.append(ID)

            # print(ID)
        frst_player = self.sql.player_extract_ID(self.app.Base.player_list[0])
        self.app.Base.online_infs[0] = frst_player['name']
        self.app.Base.online_infs[1] = frst_player['group']
        self.app.Base.online_infs[2] = frst_player['sex']
        self.app.Base.online_infs[3] = frst_player['age']
        self.app.Base.online_inf_update()
        self.parent.current = 'Tests'
        # print("----------------")


class Surepopup(BoxLayout):
    pass


class Game_box(BoxLayout):
    text = StringProperty()
    des = StringProperty()
    ID = StringProperty()

    def __init__(self, *args, **kwargs):
        super(Game_box, self).__init__(*args, **kwargs)

        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        self.text = Make_persian(self.text)
        self.des = Make_persian(self.des)
        self.ids.des_text.bind(texture_size=self.ids.des_text.setter('size'))


class Tests(Screen):
    def __init__(self, *args, **kwargs):

        super(Tests, self).__init__(*args, **kwargs)

        self.g_count = 0
        self.sql = sql()
        Clock.schedule_once(self._finish_init, -1)

    def _finish_init(self, dt):
        self.app = App.get_running_app()
        self.ids.game_page.bind(minimum_height=self.ids.game_page.setter('height'))
        self.ids.scrlv.bind(scroll_y=partial(self.slider_change, self.ids.slider))
        self.ids.slider.bind(value=partial(self.scroll_change, self.ids.scrlv))

        self.game_putter(0)

    @staticmethod
    def scroll_change(scrlv, instance, value):
        scrlv.scroll_y = value

    @staticmethod
    def slider_change(s, instance, value):
        if value >= 0:
            # this to avoid 'maximum recursion depth exceeded' error
            s.value = value

    def game_putter(self, mod):

        if mod == 0:
            items = self.sql.game_predesigned_extract_all()
            self.games = items
        else:
            return
            # self.games = [['dd-1', 'designed - 1', ' توضیحات بازی ساخته شده'],
            #               ['dd-2', 'designed - 2', ' توضیحات بازی ساخته شده'],
            #               ['dd-3', 'designed - 3', ' توضیحات بازی ساخته شده'],
            #               ['dd-4', 'designed - 4', ' توضیحات بازی ساخته شده'],
            #               ['dd-5', 'designed - 5', ' توضیحات بازی ساخته شده'],
            #               ['dd-6', 'designed - 6', ' توضیحات بازی ساخته شده'],
            #               ['dd-7', 'designed - 7', ' توضیحات بازی ساخته شده'],
            #               ['dd-8', 'designed - 8', ' توضیحات بازی ساخته شده'],
            #               ['dd-9', 'designed - 9', ' توضیحات بازی ساخته شده'],
            #               ['dd-10', 'designed - 10', ' توضیحات بازی ساخته شده'],
            #               ['dd-11', 'designed - 11', ' توضیحات بازی ساخته شده'],
            #               ['dd-12', 'designed - 12', ' توضیحات بازی ساخته شده'],
            #               ['dd-13', 'designed - 13', ' توضیحات بازی ساخته شده'],
            #               ['dd-14', 'designed - 14', ' توضیحات بازی ساخته شده'],
            #               ['dd-15', 'designed - 15', ' توضیحات بازی ساخته شده'],
            #               ['dd-16', 'designed - 16', ' توضیحات بازی ساخته شده']]

        for item in self.games:
            gm_btn = Game_box(text=item[1], des=item[2], ID=str(item[0]))
            self.ids.game_page.add_widget(gm_btn)
            # self.add_widget()

    def go_designed(self, setting):
        self.parent.current = 'Game'
        self.parent.selected_games = ["Designed"]
        self.parent.designed_setting = setting

    def start(self):
        sq = self.ids.game_page.children
        selected = list()
        for item in sq:
            state = item.ids.tgl.state
            if state == 'down':
                ID = item.ID
                s = [int(item.ids.tgl.text), item.text]
                selected.append(s)
        selected.sort()  # key=lambda x: x[0])
        s_g = ""
        for i in selected:
            s_g = s_g + str(i[0]) + " -- " + i[1] + "\n"

        bob = BoxLayout(orientation='vertical', spacing=8)
        bob.add_widget(Label_f(text="آزمون های انتخاب شده, به ترتیب:", font_size=20, color=[0, 0, 0, 1]))
        bob.add_widget(Label(text=s_g, font_size=16, bold=1, color=[0, 0, 0, 1]))
        bob.add_widget(Button(background_normal='data/icons/next.png',
                              background_down='data/icons/next.png',
                              size_hint=(None, None), size=(64, 64), on_release=self.gogo, pos_hint={'center_x': 0.5}))
        self.pop = WPopup(title="", content=bob,
                          size_hint=(None, None), size=(300, 300), pos_hint={'center_x': 0.5})
        self.pop.open()

    def gogo(self, *args):
        self.pop.dismiss()
        self.parent.current = 'Game'

    def go(self, *args):

        players = self.app.Base.player_list
        if len(players) < 1:
            pop = WPopup(title="error", content=Label_f(text="شرکت کننده ای انتخاب نشده است", color=[0, 0, 0, 1]))
            pop.opacity = .3
            pop.open()
            return

        sq = self.ids.game_page.children
        selected = list()
        for item in sq:
            state = item.ids.tgl.state
            if state == 'down':
                ID = item.ID
                s = [int(item.ids.tgl.text), item.text]
                selected.append(s)

        self.parent.selected_games = selected
        if len(selected) < 1:
            pop = WPopup(title="error", content=Label_f(text="آزمونی انتخاب نشده است", color=[0, 0, 0, 1]),
                         size_hint=(.3, .3))
            pop.open()
            return
        self.start()
        selected.sort()

    def predesigned(self):
        self.clear_games()
        self.g_count = 0
        self.game_putter(0)

    def designer(self):
        self.des_pop_contex = Designing(caller=self)
        self.des_pop = Popup(title=Make_persian("طراحی آزمون"), title_align='right', title_size=28,
                             title_font="data/BBadr.ttf", separator_color=(.4, .4, .4, 1),
                             content=self.des_pop_contex,
                             size_hint=(.9, .9), pos_hint={'center_y': .5, "center_x": .5}, auto_dismiss=False)

        self.des_pop.open()

    def designed(self):
        self.clear_games()
        self.g_count = 0
        self.game_putter(1)

    def clear_games(self):
        self.ids.game_page.clear_widgets()

    def add_game(self, state):
        if state == 'down':
            self.g_count += 1
            return str(self.g_count)
        else:
            self.g_count -= 1
            return ""


class C_Spinner(Spinner):

    def __init__(self, *args, **kwargs):
        super(C_Spinner, self).__init__(*args, **kwargs)

    pass


class SpinBox(BoxLayout):
    rdisabled = BooleanProperty()

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)

        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        self.ids.UP.bind(on_press=self.up)
        self.ids.DN.bind(on_press=self.dn)

    def up(self, val):
        if not self.rdisabled:
            if self.val < self.bound:
                self.val = self.val + self.steping if (self.val < self.max) else self.val
            else:
                self.val = self.val + self.steping2 if (self.val < self.max) else self.val

            self.val = round(self.val, 1)

    def dn(self, val):
        if not self.rdisabled:
            if self.val <= self.bound:
                self.val = self.val - self.steping if (self.val > self.min) else self.val
            else:
                self.val = self.val - self.steping2 if (self.val > self.min) else self.val

            self.val = round(self.val, 1)


# class Designer(Screen):
#
#     def __init__(self, *args, **kwargs):
#         super(Designer, self).__init__(*args, **kwargs)
#
#         Clock.schedule_once(self._finish_init, -1)
#
#     def _finish_init(self, dt):
#         self.ids.form.bind(minimum_height=self.ids.form.setter('height'))
#         # self.ids.gd_name.text = Make_persian("نام پروتکل : ")
#         # self.ids.moharek.text = Make_persian("محرک : ")
#         self.ids.moharek_type.text = Make_persian("نوع محرک")
#         self.ids.moharek_type.values = (Make_persian("دیداری"), Make_persian("شنیداری"))
#         self.ids.moharek_type_2.text = Make_persian("نوع محرک")
#         self.ids.moharek_type_2.values = (Make_persian("عدد"),
#                                           Make_persian("رنگ"),
#                                           Make_persian("شکل"),
#                                           Make_persian("فیلم"),
#                                           Make_persian("صدا"))
#         self.ids.pasokh_type.text = Make_persian("نوع پاسخ")
#         self.ids.pasokh_type.values = (Make_persian("صدا"),
#                                        Make_persian("دست"),
#                                        Make_persian("دوپا"),
#                                        Make_persian("یک پا"))
#         self.ids.pasokh_type_2.text = Make_persian("نوع پاسخ")
#         self.ids.pasokh_type_2.values = (Make_persian("عدد"),
#                                          Make_persian("رنگ"),
#                                          Make_persian("شکل"),
#                                          Make_persian("صدا"))
#         self.ids.pishneshane_type.text = Make_persian("نوع")
#         self.ids.pishneshane_type.values = (Make_persian("بالا"),
#                                             Make_persian("پایین"), Make_persian("چپ"),
#                                             Make_persian("راست"), Make_persian("جلو وعقب"),
#                                             Make_persian("چپ وراست"))
#         self.ids.pishdore_type.text = Make_persian("نوع")
#         self.ids.pishdore_type.values = (Make_persian("بوق"),
#                                          Make_persian("کلمه"),
#                                          Make_persian("روشن شدن چراغ"),
#                                          Make_persian("روشن شدن چراغ قرمز و سپس زرد"))
#         self.ids.vakonesh_time.text = Make_persian("واکنش")
#         self.ids.vakonesh_time.bind(text=self.zaman_vak)
#         self.ids.vakonesh_time.values = (Make_persian("ساده"),
#                                          Make_persian("انتخابی"),
#                                          Make_persian("افتراقی"))
#         self.ids.scrlv.bind(scroll_y=partial(self.slider_change, self.ids.slider))
#         self.ids.slider.bind(value=partial(self.scroll_change, self.ids.scrlv))
#
#     def scroll_change(self, scrlv, instance, value):
#         scrlv.scroll_y = value
#
#     def slider_change(self, s, instance, value):
#         if value >= 0:
#             # this to avoid 'maximum recursion depth exceeded' error
#             s.value = value
#
#     def zaman_vak(self, val, text):
#         if text == "ﯽﻗﺍﺮﺘﻓﺍ":
#             self.ids.pasokh_count.rdisabled = False
#         else:
#             self.ids.pasokh_count.rdisabled = True
#
#         if text == "ﻩﺩﺎﺳ":
#             self.ids.sazegary.disabled = False
#         elif (text == "ﯽﺑﺎﺨﺘﻧﺍ") & (0):
#             self.ids.sazegary.disabled = False
#         else:
#             self.ids.sazegary.disabled = True
#
#     def show_selection(self):
#
#         bob = BoxLayout(orientation='vertical', spacing=8)
#         bob.add_widget(Label_f(text="فرم انتخاب نویز", font_size=20))
#
#         self.pop = Popup(title="", content=bob,
#                          size_hint=(None, None), size=(300, 500), pos_hint={'center_x': 0.5})
#         self.pop.open()
#
#     def save(self):
#         pass


class Extractor(Screen):
    selcted_infs = dict()

    def __init__(self, *args, **kwargs):
        self.sql = sql()
        self.klist = self.Data_Extractor()

        self.catalogLA = ListAdapter(data=self.klist, args_converter=self.args_converter,
                                     cls=CItem, selection_mode='single',
                                     allow_empty_selection=True, propagate_selection_to_data=True)
        super(self.__class__, self).__init__(*args, **kwargs)

        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        self.ids['Listing'].adapter = self.catalogLA

    def on_pre_enter(self, *args):
        self.klist = self.Data_Extractor()
        self.catalogLA.data = sorted(self.klist, key=lambda item: item['crt_date'], reverse=True)

    def Data_Extractor(self):
        Data = []
        q = self.sql.player_extract_all()
        for item in q:
            var = {'name': item[1], 'age': item[2], 'crt_date': item[3], 'sex': item[4], 'group': item[5],
                   'is_selected': False, 'ID': str(item[0])}
            Data.append(var)
        return Data

    def args_converter(self, row_index, an_obj):
        return {'name': Make_persian(an_obj['name']),
                'age': an_obj['age'],
                'ID': an_obj['ID'],
                'group': Make_persian(an_obj['group']),
                'crt_date': Make_persian(an_obj['crt_date']),
                'sex': Make_persian(an_obj['sex'])}

    def sorter(self, mod):
        if mod == 'name':
            self.catalogLA.data = sorted(self.catalogLA.data, key=lambda item: item['name'])
        elif mod == 'age':
            self.catalogLA.data = sorted(self.catalogLA.data, key=lambda item: item['age'])
        elif mod == 'group':
            self.catalogLA.data = sorted(self.catalogLA.data, key=lambda item: item['group'])
        elif mod == 'sex':
            self.catalogLA.data = sorted(self.catalogLA.data, key=lambda item: item['sex'])
        elif mod == 'crt_date':
            self.catalogLA.data = sorted(self.catalogLA.data, key=lambda item: item['crt_date'], reverse=True)
        else:
            pass
        return

    def report(self):

        selecteds = self.catalogLA.selection
        if not selecteds:
            return

        try:
            ID = selecteds[0].parent.ID
        except:
            ID = selecteds[0].item.ID

        frst_player = self.sql.player_extract_ID(ID)
        self.selcted_infs = frst_player
        self.ids.doc_list_view_container.disabled = True
        self.selcted_infs['ID'] = ID
        self.ids.reporter.person_infs = self.selcted_infs
        self.ids.reporter.load()


class Setting(ButtonBehavior, Screen):
    def __init__(self, *args, **kwargs):
        self.sql = sql()
        self.klist = self.Data_Extractor()

        self.catalogLA = ListAdapter(data=self.klist, args_converter=self.args_converter,
                                     cls=adminItem, selection_mode='single',
                                     allow_empty_selection=True, propagate_selection_to_data=True)

        super(self.__class__, self).__init__(*args, **kwargs)

        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        self.ids['Listing_admin'].adapter = self.catalogLA
        return

    def Data_Extractor(self):
        Data = []
        q = self.sql.admin_extract_all()
        for item in q:
            var = {'username': item[1], 'name': item[4], 'tel': item[5], 'email': item[6], 'condition': item[7],
                   'is_selected': False, 'ID': str(item[0])}
            Data.append(var)
        return Data

    def reload_admins_list(self):
        self.catalogLA.data = self.Data_Extractor()

    def args_converter(self, row_index, an_obj):
        return {'name': Make_persian(an_obj['name']),
                'username': an_obj['username'],
                'ID': an_obj['ID'],
                # 'password': Make_persian(an_obj['password']),
                # 'crt_date': Make_persian(an_obj['crt_date']),
                'tel': Make_persian(an_obj['tel']),
                'email': Make_persian(an_obj['email']),
                'condition': Make_persian(an_obj['condition'])}

    def add_admin(self):
        admins = self.ids.admins
        admins.en = True
        # self.ids.admins_part.disabled= True

    def del_admin(self):
        q = self.catalogLA.selection
        if q != []:
            self.sql.admin_remove(q[0].text)
        self.reload_admins_list()

    def view_admin(self):
        q = self.catalogLA.selection
        if q == []: return
        print(q[0].parent.ID)

    def add_external(self):
        pass

    def del_external(self):
        pass

    def view_external(self):
        pass

    def on_press(self):
        admins = self.ids.admins
        if admins.en:
            admins.en = False
            # self.ids.admins_part.disabled= False


class admins(BoxLayout):
    sql = sql()
    en = BooleanProperty()

    def __init__(self, *args, **kwargs):

        super(self.__class__, self).__init__(*args, **kwargs)

        self.bind(en=lambda x, y: self.clear())

    def save(self):
        user = self.ids.username.text
        pass_1 = self.ids.pass_1.text
        pass_2 = self.ids.pass_2.text
        name = self.ids.name.text
        tel = self.ids.tel.text
        email = self.ids.email.text
        cond = self.ids.condition.text

        if pass_1 != pass_2:
            Popup(title=Make_persian(''), content=Label_f(text='تکرار رمز عبور اشتباه است !'),
                  size_hint=(None, None), size=(300, 200)).open()
            return
        elif user == "":
            Popup(title=Make_persian(''), content=Label_f(text='نام کاربری را وارد کنید!'),
                  size_hint=(None, None), size=(300, 200)).open()
            return
        elif self.sql.admin_check_exiting(user):
            Popup(title=Make_persian(''), content=Label_f(text='نام کاربری وجود دارد!'),
                  size_hint=(None, None), size=(300, 200)).open()
            return

        ID = self.sql.admin_add(user, pass_1, name, tel, email, cond)

        self.parent.parent.reload_admins_list()
        self.clear()
        self.en = False

    def clear(self):
        for item in self.ids:
            self.ids[item].text = ""


class Splash(Screen):
    pass


class Report_ind(ButtonBehavior, BoxLayout):
    person_infs = {'name': "", 'age': '', 'crt_dte': "", 'sex': '', 'ID': '', 'group': ''}
    P_name = StringProperty()
    P_age = StringProperty()
    P_sex = StringProperty()
    P_crt_dte = StringProperty()
    P_group = StringProperty()
    P_ID = StringProperty()

    def __init__(self, *args, **kwargs):
        self.sql = sql()
        self.klist = self.Data_Extractor()

        self.catalogLA = ListAdapter(data=self.klist, args_converter=self.args_converter,
                                     cls=gamehistoryItem, selection_mode='multiple',
                                     allow_empty_selection=True, propagate_selection_to_data=True)
        super(self.__class__, self).__init__(*args, **kwargs)

        Clock.schedule_once(self._finish_init)

    def _finish_init(self, dt):
        self.ids['Listing'].adapter = self.catalogLA
        self.ids["g_filter"].bind(text=self.g_filter)
        self.ids['az_m'].bind(focus=self.d_filter)
        self.ids['ta_m'].bind(focus=self.d_filter)
        self.ids['az_y'].bind(focus=self.d_filter)
        self.ids['ta_y'].bind(focus=self.d_filter)

    def Data_Extractor(self, iD=0):
        if iD == 0:
            Data = []
            return Data
        else:
            Data = self.sql.game_exract_played(iD)
            return Data

    def args_converter(self, row_index, an_obj):
        return {'name': an_obj['name'],
                'play_dte': an_obj['play_dte'],
                'ID': an_obj['ID'],
                'res': an_obj['res']}

    def d_filter(self, *args):
        az_m = self.ids['az_m'].text
        az_y = self.ids['az_y'].text
        ta_m = self.ids['ta_m'].text
        ta_y = self.ids['ta_y'].text

        data1 = self.Data_Extractor(self.person_infs['ID'])
        data2 = list()
        if ta_y == "" and ta_m == "":
            if az_y == "":
                return
            elif az_m == "":
                for items in data1:
                    dt = items['play_dte']
                    if dt[:4] == az_y:
                        data2.append(items)
                self.catalogLA.data = data2
            else:
                for items in data1:
                    dt = items['play_dte']
                    y = dt.split("-")[0]
                    m = dt.split("-")[1]

                    if y == az_y and m == az_m:
                        data2.append(items)

                self.catalogLA.data = data2

        elif ta_y == "" and ta_m != "":
            for items in data1:
                dt = items['play_dte']
                y = dt.split("-")[0]
                m = dt.split("-")[1]
                if y == az_y:
                    if int(az_m) <= int(m) <= int(ta_m):
                        data2.append(items)
            self.catalogLA.data = data2

        else:
            for items in data1:
                dt = items['play_dte']
                y = dt.split("-")[0]
                m = dt.split("-")[1]
                if int(az_y) <= int(y) <= int(ta_y):
                    if int(az_m) <= int(m) <= int(ta_m):
                        data2.append(items)
            self.catalogLA.data = data2

    def g_filter(self, ins, txt):

        if txt == "":
            self.catalogLA.data = self.Data_Extractor(self.person_infs['ID'])

        else:
            data1 = self.Data_Extractor(self.person_infs['ID'])
            data2 = list()
            for item in data1:
                if item['name'] == txt:
                    data2.append(item)
            self.catalogLA.data = data2

    def load(self):
        self.en = True
        self.P_name = Make_persian(self.person_infs['name'])
        self.P_age = Make_persian(self.person_infs['age'])
        self.P_crt_dte = Make_persian(self.person_infs['crt_dte'])
        self.P_sex = Make_persian(self.person_infs['sex'])
        self.P_group = Make_persian(self.person_infs['group'])
        self.P_ID = Make_persian(self.person_infs['ID'])
        self.catalogLA.data = self.Data_Extractor(self.person_infs['ID'])
        self.g_filter_creat()
        self.data_filter_creat()

    def g_filter_creat(self):
        games = [i['name'] for i in self.catalogLA.data]
        games = list(set(games))
        self.ids["g_filter"].text = ""
        self.ids["g_filter"].values = games + [""]

    def data_filter_creat(self):
        dates = [i['play_dte'] for i in self.catalogLA.data]

    def save(self):
        selecteds = self.catalogLA.selection
        if not selecteds:
            return

        try:
            ID = selecteds[0].parent.ID
        except:
            ID = selecteds[0].item.ID
        res_selected = list()
        for i in range(len(selecteds)):
            name = selecteds[i].parent.name
            play_dte = selecteds[i].parent.play_dte
            res = selecteds[i].parent.res

            r1 = res[1:-1]
            r1 = r1.replace("]", "")
            r1 = r1.replace("[", "")
            res = r1.split(",")
            res = [int(i) for i in res]

            res.append(name)
            res.append(play_dte)
            res_selected.append(res)
            # print("name = {name: ^20} , date= {play_dte: ^10}, res= {res}".format(name=name,play_dte=play_dte,res=res))
        des = {"name": self.P_name, "group": self.P_group, "crt_dte": self.P_crt_dte, "age": self.P_age,
               "sex": self.P_sex}

        XLSX.put_ready_des(des, res_selected, self.addr)

    def export(self):
        self.select_path()

    def select_path(self):
        FB = FileBrowser()
        FB.cancel_string = "Close"
        FB.select_string = "Select Folder"

        FB.bind(on_success=self._FB_submitt,
                on_canceled=self._FB_close)

        self.FB_pop = Popup(title="File Selection", content=FB, auto_dismiss=False,
                            size_hint=(.6, .9), pos_hint={"center_x": .5, "center_y": .5})
        self.FB_pop.open()

    def _FB_submitt(self, instance):
        self.addr = instance.path
        self.FB_pop.dismiss()
        self.save()
        save_pop = WPopup(title="", content=Label_f(text="فایل ذخیره شد!", color=(0, 0, 0, 1)), auto_dismiss=True,
                          size_hint=(.2, .1), pos_hint={"center_x": .5, "center_y": .5}).open()

    def _FB_close(self, ins):
        self.FB_pop.dismiss()


class Game(Screen):
    btn_state = [0 for _ in range(9)]
    trys = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    game_name = StringProperty("")
    player_num = 0
    leveltry = 0
    game_num = 0

    def __init__(self, *args, **kwargs):
        super(Game, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init)
        self.game_running = False
        self.warn_sound = SoundLoader.load("data/sounds/warn_beep.wav")

    def _finish_init(self, dt):
        self.app = App.get_running_app()
        self.ser = SER.SER()
        self.sql = sql()
        self.k_btns = [self.ids.keypad.ids["b" + str(i)] for i in range(9)]

    def start(self):
        q = self.game.start()
        if q:
            self.ids.strt_btn.disabled = True
            self.ids.rsm_btn.disabled = True
            self.ids.astp_btn.disabled = False
            self.ids.stp_btn.disabled = False
            self.game_running = True
        else:
            pass

    def stop(self):
        self.ids.rsm_btn.disabled = False
        self.ids.stp_btn.disabled = True
        self.game.pause()
        self.game_running = False

    def resume(self):
        q = self.game.resume()
        if q:
            self.ids.rsm_btn.disabled = 1
            self.ids.stp_btn.disabled = 0
            self.game_running = True

    def a_stop(self):
        self.Stop_game()
        self.game_running = False
        self.close()

    def Stop_game(self):
        self.game.stop()

    def put_try(self, react, mvmnt):
        lvl = self.leveltry
        idd = "try_" + str(lvl + 1)

        self.ids[idd + "_react"].text = str(round(react / 1000, 3))
        self.ids[idd + "_move"].text = str(round(mvmnt / 1000, 3))
        self.ids[idd + "_resp"].text = str(round((mvmnt + react) / 1000, 3))

    def empty_try(self):
        for i in range(1, 4):
            self.ids["try_{}_react".format(i)].text = str(0.00)
            self.ids["try_{}_move".format(i)].text = str(0.00)
            self.ids["try_{}_resp".format(i)].text = str(0.00)

    def set_btns(self):

        for i in range(9):
            if self.btn_state[i] == 0:
                self.k_btns[i].state = 'normal'
            else:
                self.k_btns[i].state = 'down'

    def chk(self, instance):
        btn_dict = {'0': 4, '1': 1, "2": 2, '3': 5, '4': 8, '5': 7, '6': 6, '7': 3, '8': 0}
        stt = 0
        if instance.state == 'normal':
            stt = 0
        else:
            stt = 1

        self.btn_state[btn_dict[instance.text]] = stt

    def chk_serial(self, dt):
        # res = self.ser.read(15)
        res = self.ser.read_state()
        # do everything to calculate btn state
        btn_st = res

        #######################################
        self.btn_state = btn_st
        self.set_btns()

    def errorer(self, text):
        WPopup(title="error", content=Label_f(text=text, color=[0, 0, 0, 1]), size_hint=(0.3, 0.3)).open()

    def add_try(self, react, movement):
        self.trys[self.leveltry] = [react, movement, react + movement]
        self.put_try(react, movement)
        self.ids.rsm_btn.disabled = False
        self.leveltry += 1
        self.warn_sound.play()
        if self.leveltry == 3:
            self.player_finish()
            self.leveltry = 0

    def update_try(self, react, mvmnt):
        idd = "try_" + str(self.leveltry + 1)

        self.ids[idd + "_react"].text = str(round(react / 1000, 3))
        self.ids[idd + "_move"].text = str(round(mvmnt / 1000, 3))
        self.ids[idd + "_resp"].text = str(round((mvmnt + react) / 1000, 3))

    def player_finish(self):
        # print("Finished")
        self.ids.rsm_btn.disabled = True
        self.ids.stp_btn.disabled = True
        self.ids.strt_btn.disabled = True
        self.ids.nxt_btn.disabled = False

        if (len(self.app.Base.player_list) == (self.player_num + 1)) and (
                (self.game_num + 1) == len(self.parent.selected_games)):
            self.ids.nxt_btn.disabled = True
        if (len(self.app.Base.player_list) == (self.player_num + 1)) and (self.parent.selected_games[0] == 'Designed'):
            self.game.enable_report()
            self.set_additional()

        self.save_game_res()
        self.game.reset()

    def Next(self):
        # print("Next")
        self.player_num += 1
        self.empty_try()
        self.ids.stp_btn.disabled = False
        self.ids.strt_btn.disabled = False
        self.ids.nxt_btn.disabled = True
        self.set_players()

    def next_game(self):
        if self.parent.selected_games[0] == 'Designed':
            return
        self.player_num = 0
        self.leveltry = 0
        self.game_num += 1
        if self.parent.selected_games[self.game_num][1] == 'X_O Task':
            self.game = game_classes.X_O_Task(self)
            self.game_name = self.game.name
        elif self.parent.selected_games[self.game_num][1] == '8-x Task':
            self.game = game_classes.g_x_Task(self)
            self.game_name = self.game.name
        elif self.parent.selected_games[self.game_num][1] == 'X+Y Task':
            self.game = game_classes.X_Y_Task(self)
            self.game_name = self.game.name
        elif self.parent.selected_games[self.game_num][1] == 'Implicit strop':
            self.game = game_classes.implicit_strop(self)
            self.game_name = self.game.name
        elif self.parent.selected_games[self.game_num][1] == 'Memory Task':
            self.game = game_classes.Memory_Task(self)
            self.game_name = self.game.name
        self.clear_additional()
        self.set_additional()
        self.set_players()

    def set_players(self):

        players = self.app.Base.player_list
        self.players = players
        print("player count is ", len(players), "and current player id is ", self.player_num)
        if (len(players) == self.player_num) and (len(players) > 0):
            # print("going to next game")
            self.next_game()
            return
        else:
            # print("going to next player",self.player_num)
            # return
            dct = self.sql.player_extract_ID(players[self.player_num])
            self.ids.crnt_player_name.text = Make_persian(dct['name'])
            self.ids.crnt_player_sex.text = Make_persian(dct['sex'])
            self.ids.crnt_player_birthdate.text = Make_persian(dct['age'])

        if len(players) <= (self.player_num + 1):
            self.ids.next_player_name.text = ""
            self.ids.next_player_sex.text = ""
            # self.ids.next_player_birthdate.text = ""
        else:
            dct = self.sql.player_extract_ID(players[self.player_num + 1])
            self.ids.next_player_name.text = Make_persian(dct['name'])
            self.ids.next_player_sex.text = Make_persian(dct['sex'])
            # self.ids.next_player_birthdate.text = Make_persian(dct['age'])

    def save_game_res(self):
        game_name = self.game_name
        now = datetime.datetime.now()
        game_Date = gregorian_to_jalali(now.year, now.month, now.day) + [now.hour, now.minute]
        Game_Date = str(game_Date[0]) + "-" + str(game_Date[1]) + "-" + str(game_Date[2]) + "-" + str(
            game_Date[3]) + ":" + str(game_Date[4])
        player_ID = self.app.Base.player_list[self.player_num]
        res = str((self.trys[0], self.trys[1], self.trys[2]))
        self.sql.game_res_add(player_ID, game_name, Game_Date, res)

    def on_enter(self, *args):
        sec_hndle = win32gui.FindWindow(None, "Second")
        if sec_hndle == 0:
            # subprocess.Popen([r"python", r"test_app/main.py"])
            # subprocess.Popen([r"secound/secound.exe"])
            subprocess.Popen([r"secound.exe"])
            self.server_initialize()

        self.ids.strt_btn.disabled = False
        self.ids.rsm_btn.disabled = True
        self.ids.astp_btn.disabled = True
        self.ids.stp_btn.disabled = True
        self.game_running = False

        if self.parent.selected_games[0] == "Designed":
            self.d_setting = self.parent.designed_setting
            self.game = game_classes.designed_game(self)
            self.game_name = self.game.name
            self.ids['game_name'].opacity = 0
            self.ids['game_name_lb'].opacity = 0
        else:
            self.ids['game_name'].opacity = 1
            self.ids['game_name_lb'].opacity = 1
            if self.parent.selected_games[0][1] == 'X_O Task':
                self.game = game_classes.X_O_Task(self)
                self.game_name = self.game.name
            elif self.parent.selected_games[0][1] == '8-x Task':
                self.game = game_classes.g_x_Task(self)
                self.game_name = self.game.name
            elif self.parent.selected_games[0][1] == 'X+Y Task':
                self.game = game_classes.X_Y_Task(self)
                self.game_name = self.game.name
            elif self.parent.selected_games[0][1] == 'Implicit strop':
                self.game = game_classes.implicit_strop(self)
                self.game_name = self.game.name
                pass
            elif self.parent.selected_games[0][1] == 'Memory Task':
                self.game = game_classes.Memory_Task(self)
                self.game_name = self.game.name

        if self.ser.serial_initialize():
            Clock.schedule_interval(self.chk_serial, .001)
            # return True
        else:
            print("not connected")
            WPopup(title="Error", content=Label_f(text="خطای اتصال دکمه ها", color=(0, 0, 0, 1)),
                   size_hint=(.3, .2)).open()
            self.close()

        self.set_additional()

        self.player_num = 0
        self.leveltry = 0
        self.set_players()

    def clear_additional(self):
        try:
            self.additional_box.remove_widget(self.additional_box.children[0])
        except IndexError:
            return

    def set_additional(self):
        self.additional_box = self.ids["additional_setting"]

        if not self.game.additional_setting == None:
            self.additional_box.add_widget(self.game.additional_setting)

    def on_leave(self, *args):
        # sset the manipulator of second to splash like a_stop
        # self.a_stop()
        self.Stop_game()
        self.game_running = False
        Clock.unschedule(self.chk_serial)
        self.ser.close_serial()

    def close(self):
        self.manager.current = "Splash"

    def server_initialize(self):
        self.serversocket = socket.socket()
        host = 'localhost'
        port = 22334
        self.serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.serversocket.bind(('', port))

        self.serversocket.listen(1)
        self.clientsocket, addr = self.serversocket.accept()


# class ELabel(Label):
#     en=BooleanProperty()

class Testmode(Screen):
    def __init__(self, *args, **kwargs):
        super(Testmode, self).__init__(*args, **kwargs)
        self.ser = SER.SER()

    def serial_initialize(self):
        if self.ser.serial_initialize():
            return True
        else:
            return False

    def chk_serial(self, dt):

        # do everything to calculate btn state
        btn_st = self.ser.read_state()

        #######################################
        self.btn_state = btn_st
        self.set_btn()

    def set_btn(self):
        btn_dic = {0: 8, 1: 1, 2: 2, 3: 7, 4: 0, 5: 3, 6: 6, 7: 5, 8: 4}
        for i in range(9):
            self.ids["t_" + str(btn_dic[i])].en = self.btn_state[i]

    def on_enter(self, *args):
        res = self.serial_initialize()
        if res:
            Clock.schedule_interval(self.chk_serial, 0.001)
            pass
        else:
            WPopup(title=Make_persian("خطای سریال"), content=Label_f(text="پورت serial یافت نشد!!", color=(0, 0, 0, 1)),
                   size_hint=(.3, .2)).open()

    def on_pre_leave(self, *args):
        self.ser.close_serial()
        Clock.unschedule(self.chk_serial)

        pass


def Make_persian(string):
    string = get_display(arabic_reshaper.reshape(string))
    return string


def gregorian_to_jalali(gy, gm, gd):
    g_d_m = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334]
    if gy > 1600:
        jy = 979
        gy -= 1600
    else:
        jy = 0
        gy -= 621
    if gm > 2:
        gy2 = gy + 1
    else:
        gy2 = gy
    days = (365 * gy) + (int((gy2 + 3) / 4)) - (int((gy2 + 99) / 100)) + (int((gy2 + 399) / 400)) - 80 + gd + g_d_m[
        gm - 1]
    jy += 33 * (int(days / 12053))
    days %= 12053
    jy += 4 * (int(days / 1461))
    days %= 1461
    if days > 365:
        jy += int((days - 1) / 365)
        days = (days - 1) % 365
    if days < 186:
        jm = 1 + int(days / 31)
        jd = 1 + (days % 31)
    else:
        jm = 7 + int((days - 186) / 30)
        jd = 1 + ((days - 186) % 30)
    return [jy, jm, jd]
