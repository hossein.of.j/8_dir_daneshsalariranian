import xlsxwriter
from datetime import datetime
from PIL import Image
import io
import os

def put_game_des(des: dict(), res_r: dict()):
    desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
    ur = os.path.join(desktop,"Report.xlsx")
    workbook = xlsxwriter.Workbook(ur)
    sheet = workbook.add_worksheet(name="آزمون")
    sheet.right_to_left()
    rtl = workbook.add_format({'reading_order': 2})

    sheet.set_column('A:A', 20)

    sheet.write('A1', "تاریخ انجام آزمون", rtl)

    date = str(datetime.now().year) + "-" + str(datetime.now().month) + "-" + str(datetime.now().day)

    sheet.write("B1", date)

    sheet.write('A3', "اطلاعات تنظیماتی آزمون", rtl)

    settings_title = ["نوع آزمون",
                      "تعداد محرک",
                      "نوع ارائه محرک",
                      "نوع محرک",
                      "نوع آگاه دهنده",
                      "زمان پیش دوره(ثانیه)",
                      "زمان ارائه محرک(ثانیه)",
                      "زمان بین محرک ها(ثانیه)",
                      "محرک دوگانه",
                      "نام فایل محرک دوم",
                      "ناسازگاری",
                      "موقعیت دکمه ها"]
    lst = ["az_type",
           "m_cnt",
           "m_present",
           "m_type",
           "warner",
           "pish_time",
           "m_p_time",
           "m_m_time",
           "dbl_m",
           "dbl_m_addr",
           "nasazegary"]

    if des["dbl_m_addr"]:
        des["dbl_m_addr"] = des["dbl_m_addr"].split('\\')[-1]

    game_des = [des[lst[i]] for i in range(11)]

    sheet.write_row("B3", settings_title)
    sheet.write_row("B4", game_des)

    cntr = workbook.add_format()
    cntr.set_align("center")
    sheet.set_column(1, 13, 15, cntr)

    for i in range(5):
        sheet.set_row(i, 30)

    frm = workbook.add_format()
    frm.set_border(5)
    sheet.set_row(5, 2, frm)

    img = Image.open(r"data\\btns.png")
    area = (299, 56, 615, 371)
    img = img.crop(area)
    img = img.resize((150, 150), Image.ANTIALIAS)
    iimg = io.BytesIO()
    img.save(iimg, format="PNG")
    sheet.insert_image("N2", r'data\\btns.png', {'image_data': iimg})

    # Putting results
    vcntr = workbook.add_format()
    vcntr.set_valign("vcenter")
    vcntr.set_align("center")

    if des["az_type"] == "انتخابی":
        sheet.write("A7", "نام شرکت کننده")
        tc = des["m_cnt"]
        sheet.merge_range(7, 0, 6 + (tc * 3), 0, "کوشش اول",)
        sheet.merge_range(7 + (tc * 3), 0, 6 + 2 * (tc * 3), 0, "کوشش دوم")
        sheet.merge_range(7 + 2 * (tc * 3), 0, 6 + 3 * (tc * 3), 0, "کوشش دوم")


        for i in range(tc):
            sheet.merge_range(7 + 3 * i, 1, 6 + 3 * (i + 1), 1, "محرک " + str(i + 1))
            sheet.merge_range(7 + (tc * 3) + 3 * i, 1, 6 + (tc * 3) + 3 * (i + 1), 1, "محرک " + str(i + 1))
            sheet.merge_range(7 + 2 * (tc * 3) + 3 * i, 1, 6 + 2 * (tc * 3) + 3 * (i + 1), 1, "محرک " + str(i + 1))
            sheet.set_row(6 + 3 * i,15,workbook.add_format({"bottom":2,"align": "center", "valign": "vcenter"}))
            sheet.set_row(6 + (tc * 3) + 3 * i,15,workbook.add_format({"bottom":2,"align": "center", "valign": "vcenter"}))

        s = ["زمان واکنش(ms)", "زمان جابجایی(ms)", "زمان پاسخ(ms)"] * 3 * tc
        sheet.write_column(7, 2, s)

        sheet.set_row(7, 15, workbook.add_format({"top": 5,"align": "center", "valign": "vcenter"}))
        sheet.set_row(7 + (tc * 3), 15, workbook.add_format({"top": 5,"align": "center", "valign": "vcenter"}))
        sheet.set_row(7 + 2 * (tc * 3), 15, workbook.add_format({"top": 5,"align": "center", "valign": "vcenter"}))


        for it in range(len(res_r)):
            player = res_r[it]
            if len(player) <= (tc * 9):
                for i in range((tc * 9) - len(player) + 1):
                    player.append(0)
            sheet.write_column(6, 3 + it, player)

    else:
        sheet.merge_range("A7:A8", "نام شرکت کننده")
        sheet.merge_range("B7:D7", "کوشش اول")
        sheet.merge_range("E7:G7", "کوشش دوم")
        sheet.merge_range("H7:J7", "کوشش سوم")
        s = ["زمان واکنش(ms)", "زمان جابجایی(ms)", "زمان پاسخ(ms)"] * 3
        sheet.write_row("B8", s)

        for i in range(len(res_r)):
            sheet.write_row(8 + i, 0, res_r[i])

    workbook.close()
    return


def put_ready_des(des: dict(),res_r: list(), direct):
    name=des["name"]
    group = des["group"]
    birth_date = des["age"]
    creat_date = des["crt_dte"]
    sex = des["sex"]


    # res1 = res[0:3]
    # res2 = res[3:6]
    # res3 = res[6:9]
    # g_name = res[9]
    # playdte = res[10]

    desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
    desktop = direct
    ur = os.path.join(desktop,"Report.xlsx")
    workbook = xlsxwriter.Workbook(ur)
    sheet = workbook.add_worksheet(name="آزمون")
    sheet.right_to_left()
    rtl = workbook.add_format({'reading_order': 2})

    sheet.set_row(0,20,workbook.add_format({"font_size":12,"bold":True,"align":"center"}))
    sheet.set_row(2,20,workbook.add_format({"font_size":12,"bold":True,"align":"center"}))
    # for i in range(5):
    sheet.set_column(0,2,20)

    title = ["نام و نام خانوادگی","تاریخ تولد","جنسیت","گروه","تاریخ تشکیل پرونده"]
    sheet.write_row(0, 0, title[0:3])
    sheet.write_row(2, 1, title[3:])
    sheet.set_row(1, 20, workbook.add_format({"font_size": 12, "bold": False, "align": "center"}))
    sheet.set_row(3, 20, workbook.add_format({"font_size": 12, "bold": False, "align": "center"}))
    spec = [name, birth_date, sex, group, creat_date]
    sheet.write_row(1, 0, spec[0:3])
    sheet.write_row(3, 1, spec[3:])

    sheet.set_row(4,2,workbook.add_format({"border":5}))


    sheet.set_row(6,15,workbook.add_format({"align":"center",'font_size':12,"bold":True}))
    sheet.write("B7","نام آزمون")
    sheet.write("C7","زمان آزمون")
    sheet.merge_range(5,3,5,5,"کوشش اول",workbook.add_format({"align":"center"}))
    sheet.merge_range(5,6,5,8,"کوشش دوم",workbook.add_format({"align":"center"}))
    sheet.merge_range(5,9,5,11,"کوشش سوم",workbook.add_format({"align":"center"}))
    sheet.set_column(3,11,15,workbook.add_format({"align":"center"}))
    dt = ["زمان واکنش(ms)","زمان حرکت(ms)","زمان پاسخ(ms)"] * 3
    sheet.write_row("D7",dt)

    for i in range(len(res_r)):
        games=res_r[i]
        dt = [games[9],games[10]]+games[:9]
        sheet.write_row(7+i,1,dt)




    workbook.close()
    return


setting_dict = {"az_type": "افتراقی",
                "m_cnt": 3,
                "warner": "بوق",
                "pish_time": 3.0,
                "m_present": "دیداری",
                "m_type": "جهت",
                "m_p_time": 1.0,
                "m_m_time": 2.0,
                "dbl_m": "دارد",
                "dbl_m_addr": r"data\\Test.mp3",
                "nasazegary": "ندارد",
                "btn_pos": r"data\\btns.png"}

p1 = ["حسین جعفری", 1, 2, 3, 4, 5, 6, 7, 8, 9]
p2 = ["سونیا بی نظیر", 1, 2, 3, 4, 5, 6, 7, 8, 9]
p3 = ["محمد علی کیهانی", 1, 2, 3, 4, 5, 6, 7, 8, 9]
res = [p1, p2, p3]

if __name__ == "__main__":
    put_game_des(setting_dict, res)
