import socket, random
from datetime import datetime
from threading import Thread, Event
from kivy.clock import Clock
import strings as strs
from bidi.algorithm import get_display
import arabic_reshaper
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.checkbox import CheckBox
from kivy.properties import NumericProperty, StringProperty
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.core.audio import SoundLoader
from kivy.uix.button import Button
from database_manipulator import data_base as sql

import toxlsx as XLSX

class Num_Text(TextInput):
    max_chars = NumericProperty(20)
    str = StringProperty()
    acceptable_chars = '1234567890.'

    def insert_text(self, substring, from_undo=False):
        if not (substring in self.acceptable_chars):
            return

        if not from_undo and (len(self.text) + len(substring) > self.max_chars):
            return
        self.str = self.str + substring
        self.text = Make_persian(self.str)
        substring = ""
        super(Num_Text, self).insert_text(substring, from_undo)

    def do_backspace(self, from_undo=False, mode='bkspc'):
        # super(Fa_text, self).do_backspace()
        self.str = self.str[0:len(self.str) - 1]
        self.text = Make_persian(self.str)

    def delete_selection(self):
        super(Num_Text, self).delete_selection()
        self.str = Make_persian(self.text)
        self.text = Make_persian(self.str)


class Label_f(Label):
    def __init__(self, *args, **kwargs):
        super(Label_f, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init, -1)

    def _finish_init(self, dt):
        self.text = Make_persian(self.text)


class Button_f(Button):
    def __init__(self, *args, **kwargs):
        super(Button_f, self).__init__(*args, **kwargs)
        Clock.schedule_once(self._finish_init, -1)

    def _finish_init(self, dt):
        self.text = Make_persian(self.text)


def Make_persian(string):
    string = get_display(arabic_reshaper.reshape(string))
    return string


class X_O_Task:
    name = "X_O_Task"
    situaiions = [["110000000", '2'], ["101000000", '1'], ["100010000", '4'],
                  ["100100000", '6'], ["011000000", '8'], ["010010000", '5'],
                  ["001010000", '6'], ["001001000", '4'], ["000110000", '3'],
                  ["000100100", '8'], ["000011000", '7'], ["000010100", '2'],
                  ["000010010", '1'], ["000010001", '8'], ["000001001", '2'],
                  ["000000110", '4'], ["000000011", '6']]
    game_running = False
    run_id = 0
    additional_setting = None

    def __init__(self, master):
        self.parent = master
        self.clientsocket = self.parent.clientsocket

        msg = "gst>> X_O"
        self.clientsocket.send(msg.encode())

        self.vis_time = 0.0

    def chek_starting(self):
        if self.parent.btn_state == [0, 0, 0, 0, 1, 0, 0, 0, 0]:
            return True
        return False

    def start(self):
        if self.chek_starting():
            self.pre_start()
            return True
        else:
            #
            self.parent.errorer("بازیکن در وسط بایستد")
            return False

    def stop(self):
        self.timer_off()
        msg = "fun>> stp"
        self.clientsocket.send(msg.encode())
        Clock.unschedule(self.wait_for_jump)
        Clock.unschedule(self.wait_for_land)

    def pause(self):
        self.timer_off()
        msg = "fun>> rst"
        self.clientsocket.send(msg.encode())
        Clock.unschedule(self.wait_for_jump)
        Clock.unschedule(self.wait_for_land)

    def resume(self):
        if self.chek_starting():
            if not self.parent.game_running:
                self.start()
            else:
                self.rand_select()
            return True
        else:
            self.parent.errorer("بازیکن در وسط بایستد")
            return False

    def rand_select(self):
        self.run_id = int((random.random() * datetime.now().microsecond) % 9)
        self._send()
        self.timer_on()
        Clock.schedule_interval(self.wait_for_jump, 0.001)

    def timer_on(self):
        self.f_vis_time = datetime.now()
        # self.ref_timer = Clock.schedule_interval(lambda x: self.timer_up(), .001)
        Clock.schedule_interval(self.timer_up, .001)

    def timer_off(self):
        # self.ref_timer.cancel()
        Clock.unschedule(self.timer_up)

    def timer_up(self, dt):
        tt = datetime.now() - self.f_vis_time
        self.vis_time = round(tt.total_seconds() * 1000)

        txt = str(self.vis_time / 1000.)

        u_t = txt.split(".")[1]

        if len(u_t) < 3:
            txt = txt + str("0") * (3 - len(u_t))
        self.parent.ids.ref_time.text = txt

    def start_listening(self):
        self.listening_start_time = datetime.now()
        self.th = Thread(target=self.wait_for_responde)
        self.th.start()

    def wait_for_responde(self):
        tim_out = 5
        while True:
            try:
                Data = self.clientsocket.recv(1024).decode()
            except ConnectionResetError:
                self.th.do_run = False
                return False
            if (datetime.now() - self.listening_start_time).total_seconds() > tim_out:
                print("time out riched")
                return False

            if Data == '1':
                self.rand_select()
                return True

    def _send(self):
        msg = "msg>>" + self.situaiions[self.run_id][0]
        self.clientsocket.send(msg.encode())

    def wait_for_jump(self, dt):
        if self.parent.btn_state[4] != 1:
            self.reactin_time_set()
            Clock.unschedule(self.wait_for_jump)
            Clock.schedule_interval(self.wait_for_land, 0.001)

    def wait_for_land(self, dt):

        for i in range(9):
            if self.parent.btn_state[i] != 0:
                if self.check_ans(i):
                    self.movement_time_set()
                    self.parent.add_try(self.react_time, self.movement_time)
                    self.timer_off()
                    Clock.unschedule(self.wait_for_land)

    def check_ans(self, ans):
        btn_dict = {'0': 8, '1': 1, "2": 2, '3': 7, '4': 0, '5': 3, '6': 6, '7': 5, '8': 4}
        if self.situaiions[self.run_id][1] == str(btn_dict[str(ans)]):
            return True
        else:
            False

    def pre_start(self):
        msg = "fun>> pst"
        self.clientsocket.send(msg.encode())
        self.start_listening()

    def reactin_time_set(self):
        self.react_time = self.vis_time

    def movement_time_set(self):
        self.movement_time = self.vis_time - self.react_time

    def reset(self):
        self.timer_off()
        msg = "fun>> rst"
        self.clientsocket.send(msg.encode())
        # Clock.unschedule(self.wait_for_jump)
        # Clock.unschedule(self.wait_for_land)
        pass


class g_x_Task:
    name = "8-x Task"
    situaiions = [["100000000", '0'], ["010000000", '7'], ["001000000", '6'],
                  ["000100000", '1'], ["000010000", '8'], ["000001000", '5'],
                  ["000000100", '2'], ["000000010", '3'], ["000000001", '4']]

    game_running = False
    run_id = 0
    additional_setting = None

    def __init__(self, master):
        self.parent = master
        self.clientsocket = self.parent.clientsocket

        msg = "gst>> g_x"
        self.clientsocket.send(msg.encode())
        self.vis_time = 0.0

    def chek_starting(self):
        if self.parent.btn_state == [0, 0, 0, 0, 1, 0, 0, 0, 0]:
            return True
        return False

    def start(self):
        if self.chek_starting():
            self.pre_start()
            return True
        else:
            #
            self.parent.errorer("بازیکن در وسط بایستد")
            return False

    def stop(self):
        self.timer_off()
        msg = "fun>> stp"
        self.clientsocket.send(msg.encode())
        Clock.unschedule(self.wait_for_jump)
        Clock.unschedule(self.wait_for_land)

    def pause(self):
        self.timer_off()
        msg = "fun>> rst"
        self.clientsocket.send(msg.encode())
        Clock.unschedule(self.wait_for_jump)
        Clock.unschedule(self.wait_for_land)

    def resume(self):
        if self.chek_starting():
            if not self.parent.game_running:
                self.start()
            else:
                self.rand_select()
            return True
        else:
            self.parent.errorer("بازیکن در وسط بایستد")
            return False

    def rand_select(self):
        self.run_id = int((random.random() * datetime.now().microsecond) % (len(self.situaiions)))
        self._send()
        self.timer_on()
        Clock.schedule_interval(self.wait_for_jump, 0.001)

    def timer_on(self):
        self.f_vis_time = datetime.now()
        # self.ref_timer = Clock.schedule_interval(lambda x: self.timer_up(), .001)
        Clock.schedule_interval(self.timer_up, .001)

    def timer_off(self):
        # self.ref_timer.cancel()
        Clock.unschedule(self.timer_up)

    def timer_up(self, dt):
        tt = datetime.now() - self.f_vis_time
        self.vis_time = round(tt.total_seconds() * 1000)

        txt = str(self.vis_time / 1000.)

        u_t = txt.split(".")[1]

        if len(u_t) < 3:
            txt = txt + str("0") * (3 - len(u_t))
        self.parent.ids.ref_time.text = txt

    def start_listening(self):
        self.listening_start_time = datetime.now()
        self.th = Thread(target=self.wait_for_responde)
        self.th.start()

    def wait_for_responde(self):
        tim_out = 5
        while True:
            try:
                Data = self.clientsocket.recv(1024).decode()
            except ConnectionResetError:
                self.th.do_run = False
                return False
            if (datetime.now() - self.listening_start_time).total_seconds() > tim_out:
                print("time out riched")
                return False

            if Data == '1':
                self.rand_select()
                return True

    def _send(self):
        msg = "msg>>" + self.situaiions[self.run_id][0]
        self.clientsocket.send(msg.encode())

    def wait_for_jump(self, dt):
        if self.parent.btn_state[4] != 1:
            self.reactin_time_set()
            Clock.unschedule(self.wait_for_jump)
            Clock.schedule_interval(self.wait_for_land, 0.001)

    def wait_for_land(self, dt):

        for i in range(9):
            if self.parent.btn_state[i] != 0:
                if self.check_ans(i):
                    self.movement_time_set()
                    self.parent.add_try(self.react_time, self.movement_time)
                    self.timer_off()
                    Clock.unschedule(self.wait_for_land)

    def check_ans(self, ans):
        btn_dict = {'0': 8, '1': 1, "2": 2, '3': 7, '4': 0, '5': 3, '6': 6, '7': 5, '8': 4}

        if self.situaiions[self.run_id][1] == str(btn_dict[str(ans)]):
            return True
        else:
            False

    def pre_start(self):
        msg = "fun>> pst"
        self.clientsocket.send(msg.encode())
        self.start_listening()

    def reactin_time_set(self):
        self.react_time = self.vis_time

    def movement_time_set(self):
        self.movement_time = self.vis_time - self.react_time

    def reset(self):
        self.timer_off()
        msg = "fun>> rst"
        self.clientsocket.send(msg.encode())
        # Clock.unschedule(self.wait_for_jump)
        # Clock.unschedule(self.wait_for_land)
        pass


class X_Y_Task:
    name = "X+Y Task"

    situaiions = [["100010000", '8'],
                  # for 1
                  ["011000000", '3'],
                  ["010100000", '8'],
                  ["010010000", '1'],
                  ["010001000", '4'],
                  ["010000100", '7'],
                  ["010000010", '6'],
                  ["010000001", '5'],
                  # for 2
                  ["001010000", '2'],
                  ["001001000", '5'],
                  ["001000100", '8'],
                  ["001000010", '7'],
                  ["001000001", '6'],
                  # for 3
                  ["000001001", '7'],
                  ["000001010", '8']
                  ]

    game_running = False
    run_id = 0
    additional_setting = None

    def __init__(self, master):
        self.parent = master
        self.clientsocket = self.parent.clientsocket

        msg = "gst>> g_x"
        self.clientsocket.send(msg.encode())
        self.vis_time = 0.0

    def chek_starting(self):
        if self.parent.btn_state == [0, 0, 0, 0, 1, 0, 0, 0, 0]:
            return True
        return False

    def start(self):
        if self.chek_starting():
            self.pre_start()
            return True
        else:
            #
            self.parent.errorer("بازیکن در وسط بایستد")
            return False

    def stop(self):
        self.timer_off()
        msg = "fun>> stp"
        self.clientsocket.send(msg.encode())
        Clock.unschedule(self.wait_for_jump)
        Clock.unschedule(self.wait_for_land)

    def pause(self):
        self.timer_off()
        msg = "fun>> rst"
        self.clientsocket.send(msg.encode())
        Clock.unschedule(self.wait_for_jump)
        Clock.unschedule(self.wait_for_land)

    def resume(self):
        if self.chek_starting():
            if not self.parent.game_running:
                self.start()
            else:
                self.rand_select()
            return True
        else:
            self.parent.errorer("بازیکن در وسط بایستد")
            return False

    def rand_select(self):
        self.run_id = int((random.random() * datetime.now().microsecond) % (len(self.situaiions)))
        self._send()
        self.timer_on()
        Clock.schedule_interval(self.wait_for_jump, 0.001)

    def timer_on(self):
        self.f_vis_time = datetime.now()
        Clock.schedule_interval(self.timer_up, .001)

    def timer_off(self):
        Clock.unschedule(self.timer_up)

    def timer_up(self, dt):
        tt = datetime.now() - self.f_vis_time
        self.vis_time = round(tt.total_seconds() * 1000)

        txt = str(self.vis_time / 1000.)

        u_t = txt.split(".")[1]

        if len(u_t) < 3:
            txt = txt + str("0") * (3 - len(u_t))
        self.parent.ids.ref_time.text = txt

    def start_listening(self):
        self.listening_start_time = datetime.now()
        self.th = Thread(target=self.wait_for_responde)
        self.th.start()

    def wait_for_responde(self):
        tim_out = 5
        while True:
            try:
                Data = self.clientsocket.recv(1024).decode()
            except ConnectionResetError:
                self.th.do_run = False
                return False
            if (datetime.now() - self.listening_start_time).total_seconds() > tim_out:
                print("time out riched")
                return False

            if Data == '1':
                self.rand_select()
                return True

    def _send(self):
        msg = "msg>>" + self.situaiions[self.run_id][0]
        self.clientsocket.send(msg.encode())

    def wait_for_jump(self, dt):
        if self.parent.btn_state[4] != 1:
            self.reactin_time_set()
            Clock.unschedule(self.wait_for_jump)
            Clock.schedule_interval(self.wait_for_land, 0.001)

    def wait_for_land(self, dt):

        for i in range(9):
            if self.parent.btn_state[i] != 0:
                if self.check_ans(i):
                    self.movement_time_set()
                    self.parent.add_try(self.react_time, self.movement_time)
                    self.timer_off()
                    Clock.unschedule(self.wait_for_land)

    def check_ans(self, ans):
        btn_dict = {'0': 8, '1': 1, "2": 2, '3': 7, '4': 0, '5': 3, '6': 6, '7': 5, '8': 4}

        if self.situaiions[self.run_id][1] == str(btn_dict[str(ans)]):
            return True
        else:
            False

    def pre_start(self):
        msg = "fun>> pst"
        self.clientsocket.send(msg.encode())
        self.start_listening()

    def reactin_time_set(self):
        self.react_time = self.vis_time

    def movement_time_set(self):
        self.movement_time = self.vis_time - self.react_time

    def reset(self):
        self.timer_off()
        msg = "fun>> rst"
        self.clientsocket.send(msg.encode())
        # Clock.unschedule(self.wait_for_jump)
        # Clock.unschedule(self.wait_for_land)


class implicit_strop:
    name = "Implicit Strop"
    cols_list = ['Black', 'Green', 'Red', 'Blue', 'Yellow', 'Cyan', 'Magenta', 'Gray']
    cols_code = {'Black': (0, 0, 0, 1), 'Green': (0, 1, 0, 1), 'Red': (1, 0, 0, 1), 'Blue': (0, 0, 1, 1),
                 'Yellow': (1, 1, 0, 1), 'Cyan': (0, 1, 1, 1),
                 'Magenta': (1, 0, 1, 1), 'Gray': (.6, .6, .6, 1)}
    cols_fa = {'Black': Make_persian('سیاه'), "Green": Make_persian("سبز"), "Red": Make_persian("قرمز"),
               "Blue": Make_persian("آبی"), 'Yellow': Make_persian("زرد"), 'Cyan': Make_persian("نیلی"),
               'Magenta': Make_persian("سرخابی"), 'Gray': Make_persian("خاکستری")}

    game_running = False
    run_id = 0
    additional_setting = None

    def __init__(self, master):
        self.parent = master
        self.clientsocket = self.parent.clientsocket

        msg = "gst>> I_S"
        self.clientsocket.send(msg.encode())

        self.vis_time = 0.0
        self.setting_maker()

    def setting_maker(self):
        self.additional_setting = BoxLayout(orientation="vertical", padding=5, spacing=8)
        self.col_count = 1
        self.nasazegary = False
        lb1 = Label_f(text="تعداد رنگ", color=(0, 0, 0, 1), pos_hint={"right": 1}, font_size=16)
        bx1 = BoxLayout(padding=[10, 0], spacing=4)
        self.col_nums = [CheckBox(group="col_nam", color=(0, 0, 0, 4)),
                         CheckBox(group="col_nam", color=(0, 0, 0, 4)),
                         CheckBox(group="col_nam", color=(0, 0, 0, 4), active=True)]
        self.col_nums[0].bind(active=self.on_cols_num_active)
        self.col_nums[1].bind(active=self.on_cols_num_active)
        self.col_nums[2].bind(active=self.on_cols_num_active)
        bx1.add_widget(Label_f(text="3", color=(0, 0, 0, 1)))
        bx1.add_widget(self.col_nums[0])
        bx1.add_widget(Label_f(text="2", color=(0, 0, 0, 1)))
        bx1.add_widget(self.col_nums[1])
        bx1.add_widget(Label_f(text="1", color=(0, 0, 0, 1)))
        bx1.add_widget(self.col_nums[2])

        bx2 = BoxLayout(padding=[10, 0], spacing=4)
        bx2.add_widget(Label_f(text="نا سازگاری", color=(0, 0, 0, 1)))
        self.nasazegary_chek = CheckBox(color=(0, 0, 0, 4))
        self.nasazegary_chek.bind(active=self.nasazegary_set)
        bx2.add_widget(self.nasazegary_chek)

        bx3 = BoxLayout(padding=[10, 0], spacing=4)
        self.wait_time = Num_Text(text="3.5", size_hint_x=0.3, font_size=18)
        bx3.add_widget(self.wait_time)
        bx3.add_widget(Label_f(text="زمان نمایش پیش نشانه(s):", font_size=14, color=(0, 0, 0, 1)))

        self.additional_setting.add_widget(lb1)
        self.additional_setting.add_widget(bx1)
        self.additional_setting.add_widget(bx2)
        self.additional_setting.add_widget(bx3)
        self.additional_setting.add_widget(Label())

    def nasazegary_set(self, chk, val):
        if val:
            self.nasazegary = True
        else:
            self.nasazegary = False

    def on_cols_num_active(self, chk, val):
        if chk == self.col_nums[0] and val:
            self.col_count = 3
        elif chk == self.col_nums[1] and val:
            self.col_count = 2
        elif chk == self.col_nums[2] and val:
            self.col_count = 1

    def chek_starting(self):
        if self.parent.btn_state == [0, 0, 0, 0, 1, 0, 0, 0, 0]:
            return True
        return False

    def start(self):
        if self.chek_starting():
            self.answers = [0 for _ in range(self.col_count)]
            self.lvl = 0
            self.pre_screen()
            return True
        else:
            #
            self.parent.errorer("بازیکن در وسط بایستد")
            return False

    def stop(self):
        self.timer_off()
        msg = "fun>> stp"
        self.clientsocket.send(msg.encode())
        Clock.unschedule(self.wait_for_jump)
        Clock.unschedule(self.wait_for_land)

    def pause(self):
        self.timer_off()
        # msg = "fun>> rst"
        # self.clientsocket.send(msg.encode())
        self.clear_table()
        Clock.unschedule(self.wait_for_jump)
        Clock.unschedule(self.wait_for_land)

    def resume(self):
        if self.chek_starting():
            self.answers = [0 for _ in range(self.col_count)]
            self.lvl = 0
            self.pre_screen()
            return True
        else:
            self.parent.errorer("بازیکن در وسط بایستد")
            return False

    def rand_select(self):
        rnds = list()
        for i in range(8):
            #     self.run_id = int((random.random() * datetime.now().microsecond) % 8)
            rnds.append(self.cols_code[self.cols_list[i]])
        random.shuffle(rnds)

        self._send()
        self.timer_on()
        Clock.schedule_interval(self.wait_for_jump, 0.001)

    def timer_on(self):
        self.f_vis_time = datetime.now()
        # self.ref_timer = Clock.schedule_interval(lambda x: self.timer_up(), .001)
        Clock.schedule_interval(self.timer_up, .001)

    def timer_off(self):
        # self.ref_timer.cancel()
        Clock.unschedule(self.timer_up)

    def timer_up(self, dt):
        tt = datetime.now() - self.f_vis_time
        self.vis_time = round(tt.total_seconds() * 1000)

        txt = str(self.vis_time / 1000.)

        u_t = txt.split(".")[1]

        if len(u_t) < 3:
            txt = txt + str("0") * (3 - len(u_t))
        self.parent.ids.ref_time.text = txt

    def start_listening(self):
        self.listening_start_time = datetime.now()
        self.th = Thread(target=self.wait_for_responde)
        self.th.start()

    def wait_for_responde(self):
        tim_out = 5
        while True:
            try:
                Data = self.clientsocket.recv(1024).decode()
            except ConnectionResetError:
                self.th.do_run = False
                return False
            if (datetime.now() - self.listening_start_time).total_seconds() > tim_out:
                print("time out riched")
                return False

            if Data == '1':
                self.rand_select()
                return True

    def _send(self):
        rnd_col = self.cols_list.copy()
        random.shuffle(rnd_col)
        for i in range(8):
            rnd_col[i] = self.cols_code[rnd_col[i]]

        self.ans_pos = {0: rnd_col[0], 1: rnd_col[1], 2: rnd_col[2], 3: rnd_col[3], 5: rnd_col[4], 6: rnd_col[5],
                        7: rnd_col[6], 8: rnd_col[7]}

        msg = "msg>>" + str(rnd_col)
        self.clientsocket.send(msg.encode())

    def wait_for_jump(self, dt):
        if self.parent.btn_state[4] != 1:
            self.reactin_time_set()
            Clock.unschedule(self.wait_for_jump)
            Clock.schedule_interval(self.wait_for_land, 0.001)

    def wait_for_land(self, dt):

        for i in range(9):
            if self.parent.btn_state[i] != 0:
                if self.check_ans(i):
                    self.answers[self.lvl] = 1
                    if all(self.answers):
                        self.movement_time_set()
                        self.parent.add_try(self.react_time, self.movement_time)
                        self.timer_off()
                        Clock.unschedule(self.wait_for_land)
                    else:
                        self.lvl += 1

    def check_ans(self, ans):
        btn_dict = {'0': 8, '1': 1, "2": 2, '3': 7, '4': 0, '5': 3, '6': 6, '7': 5, '8': 4}
        T_col = [self.cols_code[self.T_ans[i]] for i in range(self.col_count)]
        if ans == 4: return False
        if self.ans_pos[ans] == T_col[self.lvl]:
            return True
        else:
            return False

    def clear_table(self):
        rnd_col = list()
        for i in range(8):
            rnd_col.append((1,1,1,1))

        self.ans_pos = {0: rnd_col[0], 1: rnd_col[1], 2: rnd_col[2], 3: rnd_col[3], 5: rnd_col[4], 6: rnd_col[5],
                        7: rnd_col[6], 8: rnd_col[7]}

        msg = "msg>>" + str(rnd_col)
        self.clientsocket.send(msg.encode())

    def pre_screen(self):
        dt = ""
        self.T_ans = list()

        for i in range(self.col_count):
            self.run_id = int((random.random() * datetime.now().microsecond) % 8)

            while True:
                if self.cols_list[self.run_id] in self.T_ans:
                    self.run_id = int((random.random() * datetime.now().microsecond) % 8)
                else:
                    break

            if not self.nasazegary:
                self.run_id_2 = self.run_id

            else:
                self.run_id_2 = int((random.random() * datetime.now().microsecond) % 8)
                while True:
                    if self.run_id_2 == self.run_id:
                        self.run_id_2 = int((random.random() * datetime.now().microsecond) % 8)
                    else:
                        break

            data = [self.cols_fa[self.cols_list[self.run_id]], self.cols_code[self.cols_list[self.run_id_2]]]

            self.T_ans.append(self.cols_list[self.run_id_2])

            dt = dt + ";" + data[0] + ":" + str(data[1])

        for i in range(3 - self.col_count):
            dt = dt + ";" + "" + ":" + str((0, 0, 0, 0))

        msg = "fun>> psc"

        msg = msg + dt
        self.clientsocket.send(msg.encode())
        time_to_wait = float(self.wait_time.text)
        Clock.schedule_once(lambda x: self.pre_start(), time_to_wait)

    def pre_start(self):
        msg = "fun>> pst"
        self.clientsocket.send(msg.encode())
        self.start_listening()

    def reactin_time_set(self):
        self.react_time = self.vis_time

    def movement_time_set(self):
        self.movement_time = self.vis_time - self.react_time

    def reset(self):
        self.timer_off()
        msg = "fun>> rst"
        self.clientsocket.send(msg.encode())
        # Clock.unschedule(self.wait_for_jump)
        # Clock.unschedule(self.wait_for_land)
        pass


class Memory_Task:
    name = "Memory Task"
    rand_count = 3
    rand_time = .5
    additional_setting = None

    def __init__(self, master):
        self.parent = master
        self.clientsocket = self.parent.clientsocket

        msg = "gst>> M_T"
        self.clientsocket.send(msg.encode())
        self.vis_time = 0.0
        self.complexity = False
        self.Make_setting()

    def Make_setting(self):
        self.additional_setting = BoxLayout(orientation="vertical", padding=5, spacing=8)

        bx1 = BoxLayout(padding=[10, 0], spacing=4)
        self.motivator_count_text = Num_Text(text="1", font_size=20, size_hint_x=.4)
        bx1.add_widget(self.motivator_count_text)
        bx1.add_widget(Label_f(text="تعداد محرک:", color=(0, 0, 0, 1)))

        bx2 = BoxLayout(padding=[10, 0], spacing=4)
        self.show_time_text = Num_Text(text="1.0", font_size=20, size_hint_x=.4)
        bx2.add_widget(self.show_time_text)
        bx2.add_widget(Label_f(text="زمان نمایش محرک ها(s):", color=(0, 0, 0, 1), font_size=14))

        bx3 = BoxLayout(padding=[10, 0], spacing=4)
        bx3.add_widget(Label_f(text="پیچیده", color=(0, 0, 0, 1)))
        self.complexity_chek = CheckBox(color=(0, 0, 0, 4))
        self.complexity_chek.bind(active=self.complexity_set)
        bx3.add_widget(self.complexity_chek)

        self.additional_setting.add_widget(bx1)
        self.additional_setting.add_widget(bx2)
        self.additional_setting.add_widget(bx3)
        self.additional_setting.add_widget(Label())

    def complexity_set(self, chk, val):
        if val:
            self.complexity = True
        else:
            self.complexity = False

    def chek_starting(self):
        if self.parent.btn_state == [0, 0, 0, 0, 1, 0, 0, 0, 0]:
            return True
        return False

    def start(self):
        if self.chek_starting():
            self.answers = [0 for _ in range(int(self.motivator_count_text.text))]
            self.lvl = 0
            self.pre_screen()
            return True
        else:
            #
            self.parent.errorer("بازیکن در وسط بایستد")
            return False

    def stop(self):
        self.timer_off()
        msg = "fun>> stp"
        self.clientsocket.send(msg.encode())
        Clock.unschedule(self.wait_for_jump)
        Clock.unschedule(self.wait_for_land)

    def pause(self):
        self.timer_off()
        msg = "fun>> rst"
        self.clientsocket.send(msg.encode())
        Clock.unschedule(self.wait_for_jump)
        Clock.unschedule(self.wait_for_land)

    def resume(self):
        if self.chek_starting():
            self.answers = [0 for _ in range(int(self.motivator_count_text.text))]
            self.lvl = 0
            self.pre_screen()
            return True
        else:
            #
            self.parent.errorer("بازیکن در وسط بایستد")
            return False

    def rand_select(self):
        # rnds = list()
        # for i in range(8):
        #     #     self.run_id = int((random.random() * datetime.now().microsecond) % 8)
        #     rnds.append(self.cols_code[self.cols_list[i]])
        # random.shuffle(rnds)
        #
        # self._send()
        self.timer_on()
        Clock.schedule_interval(self.wait_for_jump, 0.001)
        pass

    def timer_on(self):
        self.f_vis_time = datetime.now()
        # self.ref_timer = Clock.schedule_interval(lambda x: self.timer_up(), .001)
        Clock.schedule_interval(self.timer_up, .001)

    def timer_off(self):
        # self.ref_timer.cancel()
        Clock.unschedule(self.timer_up)

    def timer_up(self, dt):
        tt = datetime.now() - self.f_vis_time
        self.vis_time = round(tt.total_seconds() * 1000)

        txt = str(self.vis_time / 1000.)

        u_t = txt.split(".")[1]

        if len(u_t) < 3:
            txt = txt + str("0") * (3 - len(u_t))
        self.parent.ids.ref_time.text = txt

    def start_listening(self):
        self.listening_start_time = datetime.now()
        self.th = Thread(target=self.wait_for_responde)
        self.th.start()

    def wait_for_responde(self):
        tim_out = 5
        while True:
            try:
                Data = self.clientsocket.recv(1024).decode()
            except ConnectionResetError:
                self.th.do_run = False
                return False
            if (datetime.now() - self.listening_start_time).total_seconds() > tim_out:
                print("time out riched")
                return False

            if Data == '1':
                self.rand_select()
                return True

    def _send(self):
        rnd_col = self.cols_list.copy()
        random.shuffle(rnd_col)
        for i in range(8):
            rnd_col[i] = self.cols_code[rnd_col[i]]

        self.ans_pos = {0: rnd_col[0], 1: rnd_col[1], 2: rnd_col[2], 3: rnd_col[3], 5: rnd_col[4], 6: rnd_col[5],
                        7: rnd_col[6], 8: rnd_col[7]}

        msg = "msg>>" + str(rnd_col)
        self.clientsocket.send(msg.encode())

    def wait_for_jump(self, dt):
        if self.parent.btn_state[4] != 1:
            self.reactin_time_set()
            Clock.unschedule(self.wait_for_jump)
            Clock.schedule_interval(self.wait_for_land, 0.001)

    def wait_for_land(self, dt):

        for i in range(9):
            if self.parent.btn_state[i] != 0:
                if self.check_ans(i):
                    self.answers[self.lvl] = 1
                    if all(self.answers):
                        self.movement_time_set()
                        self.parent.add_try(self.react_time, self.movement_time)
                        self.timer_off()
                        Clock.unschedule(self.wait_for_land)
                    else:
                        self.lvl += 1

    def check_ans(self, ans):
        btn_dict = {'0': 8, '1': 1, "2": 2, '3': 7, '4': 0, '5': 3, '6': 6, '7': 5, '8': 4}
        # if ans == 4: return False
        if self.complexity:
            if ans == self.T_ans[self.lvl]:
                return True
            else:
                return False
        else:
            if ans in self.T_ans:
                self.T_ans.remove(ans)
                return True
            else:
                return False

    def pre_screen(self):
        dt = ""
        self.T_ans = list()

        # if self.complexity:
        #     pass
        # else:
        rnd_cnt = int(self.motivator_count_text.text)
        if rnd_cnt == 0:
            rnd_cnt = 1
        dt = "["
        for i in range(rnd_cnt):
            self.run_id = int((random.random() * datetime.now().microsecond) % 8)
            while True:
                if self.run_id in self.T_ans:
                    self.run_id = int((random.random() * datetime.now().microsecond) % 8)
                else:
                    break

            self.T_ans.append(self.run_id)
            dt = dt + str(self.run_id) + ":" + str(i+1) + ","

        dt = dt + "]"
        msg = "msg>>"
        msg = msg + dt
        self.clientsocket.send(msg.encode())
        time_to_wait = float(self.show_time_text.text)
        Clock.schedule_once(lambda x: self.pre_start(), time_to_wait)

    def pre_start(self):
        msg = "msg>>[1: ]"
        self.clientsocket.send(msg.encode())
        msg = "fun>> pst"
        self.clientsocket.send(msg.encode())
        self.start_listening()

    def reactin_time_set(self):
        self.react_time = self.vis_time

    def movement_time_set(self):
        self.movement_time = self.vis_time - self.react_time

    def reset(self):
        self.timer_off()
        msg = "fun>> rst"
        self.clientsocket.send(msg.encode())
        # Clock.unschedule(self.wait_for_jump)
        # Clock.unschedule(self.wait_for_land)
        pass


class designed_game:
    name = "Custom Test"
    num_sound_aadr = {1:"data/sounds/1.wav",2:"data/sounds/2.wav",3:"data/sounds/3.wav",4:"data/sounds/4.wav",
                    5:"data/sounds/5.wav",6:"data/sounds/6.wav",7:"data/sounds/7.wav",8:"data/sounds/8.wav"}
    dir_sound_aadr = {1: "data/sounds/N.wav", 2: "data/sounds/NE.wav", 3: "data/sounds/E.wav", 4: "data/sounds/SE.wav",
                      5: "data/sounds/S.wav", 6: "data/sounds/SW.wav", 7: "data/sounds/W.wav", 8: "data/sounds/NW.wav"}
    col_sound_aadr = {1: "data/sounds/Black.wav", 2: "data/sounds/Yellow.wav", 3: "data/sounds/Green.wav",
                      4: "data/sounds/Cyan.wav",  5: "data/sounds/White.wav",  6: "data/sounds/Blue.wav",
                      7: "data/sounds/Magenta.wav", 8: "data/sounds/Red.wav"}

    btn_2_col = {1: "Black", 2: "Yellow", 3: "Green",
                      4: "Cyan",  5: "White",  6: "Blue",
                      7: "Magenta", 8: "Red"}
    btn_2_dir = {1:"N", 2:"NE", 3:"E", 4:"SE",5:"S",6:"SW",7:"W",8:"NW"}

    game_running = False
    run_id = 0
    additional_setting = None

    double_motivator_satate = False
    double_motivator = None
    players_data = list()

    def __init__(self, master):
        self.parent = master
        self.clientsocket = self.parent.clientsocket

        self.setting = self.parent.d_setting
        btn_pos = self.setting["btn_pos"]

        self.initial_msgs([["gst>> DSN",0],
                           ["fun>> sbp" + str(btn_pos),0],
                           ["fun>> kms" + str(self.setting["m_type"]),0],
                           ["fun>> nps",0],
                           ["msg>>"+"1,0,0,0,0,0,0,0,0",2]])

        self.ready_sound = SoundLoader.load("data/sounds/ready_beep.wav")
        self.go_sound = SoundLoader.load("data/sounds/go_beep.wav")
        self.warn_sound = SoundLoader.load("data/sounds/warn_beep.wav")
        self.vis_time = 0.0

    def initial_msgs(self, msgs : list):
        msg = msgs[0][0]
        self.clientsocket.send(msg.encode())
        if len(msgs) > 1:
            msgs.pop(0)
            s_t = msgs[0][1]
            if s_t == 0:
                self.sender_clck = Clock.schedule_once(lambda x: self.initial_msgs(msgs),0.05)
            else:
                self.sender_clck = Clock.schedule_once(lambda x: self.initial_msgs(msgs),s_t)

    def chek_starting(self):
        if self.parent.btn_state == [0, 0, 0, 0, 1, 0, 0, 0, 0]:
            return True
        return False

    def start(self):
        if self.chek_starting():
            self.ress = list()
            self.vid_source_seted = False
            if self.setting["az_type"] == 0:

                self.answers = [0 for _ in range(1)]
                self.lvl = 0

                self.pre_start()

                return True
            elif self.setting["az_type"] == 1:
                self.m_cnt = self.setting["m_cnt"]
                self.answers = [0 for _ in range(self.m_cnt)]
                self.ans_dt = [[0, 0, 0] for _ in range(self.m_cnt)]
                self.lvl = 0
                self.rnds = list() #[0 for _ in range(self.m_cnt)]
                btn_list = self.check_enabled_btn()[1:]
                for i in range(self.m_cnt):
                    r = random.choice(btn_list)
                    if i != 0 and (r == self.rnds[i - 1]):
                        while r == self.rnds[i-1]:
                            r = random.choice(btn_list)
                    self.rnds.append(r)
                self.pre_start()
                return
            else:
                self.m_cnt = self.setting["m_cnt"]
                self.answers = [0]
                self.lvl = 0
                btn_list = self.check_enabled_btn()[1:]
                self.T_ans = random.choice(btn_list)

                btn_list.remove(self.T_ans)

                if self.setting["m_type"] == 0:
                    msg = "fun>> psc0"
                    msg = msg + str(self.T_ans)
                    self.clientsocket.send(msg.encode())

                elif self.setting["m_type"] == 1:
                    msg = "fun>> psc2"
                    msg = msg + self.btn_2_dir[self.T_ans]
                    self.clientsocket.send(msg.encode())

                else:
                    msg = "fun>> psc1"
                    msg = msg + self.btn_2_col[self.T_ans]
                    self.clientsocket.send(msg.encode())
                self.rnds = list()
                for i in range(self.m_cnt - 1):
                    self.rnds.append(random.choice(btn_list))
                    while (self.rnds[i] == self.rnds[i-1]) and i >= 1:
                        self.rnds[i] = random.choice(btn_list)


                self.rnds.append(self.T_ans)

                print(self.rnds)

                Clock.schedule_once(lambda x: self.pre_start(), 3)
        else:
            #
            self.parent.errorer("بازیکن در وسط بایستد")
            return False

    def stop(self):
        self.timer_off()
        msg = "fun>> stp"
        self.clientsocket.send(msg.encode())
        try:
            self.double_motivator.stop()
            self.double_motivator_satate = False
        except:
            msg = "fun>> svmS"
            self.clientsocket.send(msg.encode())
            self.double_motivator_satate = False
        Clock.unschedule(self.wait_for_jump)
        Clock.unschedule(self.wait_for_land)

    def pause(self):
        self.timer_off()
        # msg = "fun>> rst"
        # self.clientsocket.send(msg.encode())
        self.clear_table()
        try:
            self.double_motivator.stop()
            self.double_motivator_satate = False
        except:
            msg = "fun>> svmR"
            self.clientsocket.send(msg.encode())
            self.double_motivator_satate = False
        Clock.unschedule(self.wait_for_jump)
        Clock.unschedule(self.wait_for_land)

    def resume(self):
        if self.chek_starting():
            if self.setting["az_type"] == 0:
                self.answers = [0 for _ in range(1)]
                self.lvl = 0
                self.pre_start()
                return True
            elif self.setting["az_type"] == 1:
                # self.m_cnt = self.setting["m_cnt"]
                self.answers = [0 for _ in range(self.m_cnt)]
                self.ans_dt = [[0, 0, 0] for _ in range(self.m_cnt)]
                self.lvl = 0
                self.rnds = list()  # [0 for _ in range(self.m_cnt)]
                btn_list = self.check_enabled_btn()[1:]
                for i in range(self.m_cnt):
                    r = random.choice(btn_list)
                    if i != 0 and (r == self.rnds[i - 1]):
                        while r == self.rnds[i - 1]:
                            r = random.choice(btn_list)
                    self.rnds.append(r)
                self.pre_start()
                return
            else:
                self.m_cnt = self.setting["m_cnt"]
                self.answers = [0]
                self.lvl = 0
                btn_list = self.check_enabled_btn()[1:]
                self.T_ans = random.choice(btn_list)

                btn_list.remove(self.T_ans)

                if self.setting["m_type"] == 0:
                    msg = "fun>> psc0"
                    msg = msg + str(self.T_ans)
                    self.clientsocket.send(msg.encode())

                elif self.setting["m_type"] == 1:
                    msg = "fun>> psc2"
                    msg = msg + self.btn_2_dir[self.T_ans]
                    self.clientsocket.send(msg.encode())

                else:
                    msg = "fun>> psc1"
                    msg = msg + self.btn_2_col[self.T_ans]
                    self.clientsocket.send(msg.encode())

                self.rnds = list()
                for i in range(self.m_cnt - 1):
                    self.rnds.append(random.choice(btn_list))
                    while (self.rnds[i] == self.rnds[i - 1]) and i >= 1:
                        self.rnds[i] = random.choice(btn_list)
                self.rnds.append(self.T_ans)

                print(self.rnds)

                Clock.schedule_once(lambda x: self.pre_start(), 3)


                pass
        else:
            #
            self.parent.errorer("بازیکن در وسط بایستد")
            return False

    def check_enabled_btn(self):
        btn_pos = self.setting["btn_pos"]
        btn_en = [0 for _ in range(9)]
        k = list()
        for i in range(9):
            if btn_pos[i] != (0,0):
                btn_en[i] = 1
                k.append(i)
        self.btn_en = btn_en
        return k

    def rand_select(self):
        m_p_type = self.setting["m_present"]
        m_type = self.setting["m_type"]

        if self.setting["dbl_m"]:
            if not self.double_motivator_satate:
                if self.setting['m_present'] == 0:
                    self.double_motivator = SoundLoader.load(self.setting["dbl_m_addr"])
                    self.double_motivator.play()
                    self.double_motivator_satate = True
                else:
                    if not self.vid_source_seted:
                        addr = self.setting["dbl_m_addr"]
                        msg1 = ["fun>> " + "svs" + addr, 0]
                        # self.clientsocket.send(msg1[0].encode())
                        msg2 = ["fun>> svmP", 0.1]
                        self.vid_source_seted = True
                        self.initial_msgs([msg1,msg2])
                        self.double_motivator_satate = True
                    else:
                        msg = "fun>> svmP"
                        self.clientsocket.send(msg.encode())
                        self.double_motivator_satate = True

        btn_list = self.check_enabled_btn()

        if self.setting["az_type"] == 0:
            s = random.randint(1, len(btn_list) - 1)
            btn_to_do = btn_list[s]
            self.T_ans = btn_to_do

            if m_p_type == 0:
                dt = "1"
                for i in range(1, 9):
                    if i != btn_to_do:
                        dt = dt + ","+"0"
                    else:
                        dt = dt + "," + "1"

                msg = "msg>>" + dt
                self.clientsocket.send(msg.encode())
                self.timer_on()
                Clock.schedule_interval(self.wait_for_jump, 0.001)

            else:
                if m_type == 0:
                    self.s_player = SoundLoader.load(self.num_sound_aadr[btn_to_do])
                    length = self.s_player.length
                    self.s_player.play()
                    Clock.schedule_once(lambda x: self.do_waiter_sound(),length)

                elif m_type == 1:
                    self.s_player = SoundLoader.load(self.dir_sound_aadr[btn_to_do])
                    length = self.s_player.length
                    self.s_player.play()
                    Clock.schedule_once(lambda x: self.do_waiter_sound(), length)

                elif m_type == 2:
                    self.s_player = SoundLoader.load(self.col_sound_aadr[btn_to_do])
                    length = self.s_player.length
                    self.s_player.play()
                    Clock.schedule_once(lambda x: self.do_waiter_sound(), length)

        elif self.setting["az_type"] == 1:
            self.m_p_time = self.setting["m_p_time"]
            target = self.rnds[self.lvl]
            if m_p_type == 0:

                dt = "1"
                for i in range(1, 9):
                    if i != target:
                        dt = dt + "," + "0"
                    else:
                        dt = dt + "," + "1"

                msg = "msg>>" + dt
                self.clientsocket.send(msg.encode())
                self.timer_on()
                Clock.schedule_once(self.clear_table, self.m_p_time)
                Clock.schedule_interval(self.wait_for_jump, 0.001)

            else:
                if m_type == 0:
                    self.s_player = SoundLoader.load(self.num_sound_aadr[target])
                    length = self.s_player.length
                    self.s_player.play()
                    Clock.schedule_once(lambda x: self.do_waiter_sound(), length)
                elif m_type == 1:
                    self.s_player = SoundLoader.load(self.dir_sound_aadr[target])
                    length = self.s_player.length
                    self.s_player.play()
                    Clock.schedule_once(lambda x: self.do_waiter_sound(), length)
                elif m_type == 2:
                    self.s_player = SoundLoader.load(self.col_sound_aadr[target])
                    length = self.s_player.length
                    self.s_player.play()
                    Clock.schedule_once(lambda x: self.do_waiter_sound(), length)

        else:
            self.m_p_time = self.setting["m_p_time"]
            if m_p_type == 0:
                if self.lvl == (self.m_cnt - 1):
                    target = self.T_ans
                    dt = "1"
                    for i in range(1, 9):
                        if i != target:
                            dt = dt + "," + "0"
                        else:
                            dt = dt + "," + "1"

                    msg = "msg>>" + dt
                    self.clientsocket.send(msg.encode())
                    self.timer_on()
                    self.clear_timer = Clock.schedule_once(self.clear_table, self.m_p_time)
                    Clock.schedule_interval(self.wait_for_jump, 0.001)
                else:
                    target = self.rnds[self.lvl]
                    dt = "1"
                    for i in range(1, 9):
                        if i != target:
                            dt = dt + "," + "0"
                        else:
                            dt = dt + "," + "1"

                    msg = "msg>>" + dt
                    self.clientsocket.send(msg.encode())
                    if self.m_p_time < self.setting["m_m_time"]:
                        Clock.schedule_once(self.clear_table, self.m_p_time)

                    Clock.schedule_interval(self.wait_for_jump, 0.001)
                    self.lvl += 1
                    self.rnd_sel_clck = Clock.schedule_once(lambda x: self.rand_select(), self.setting["m_m_time"])
            else:
                selected_mod = None
                if m_type == 0:
                    selected_mod = self.num_sound_aadr
                elif m_type == 1:
                    selected_mod = self.dir_sound_aadr
                elif m_type == 2:
                    selected_mod = self.col_sound_aadr

                if self.lvl == (self.m_cnt - 1):

                    target = self.T_ans
                    self.s_player = SoundLoader.load(selected_mod[target])
                    length = self.s_player.length
                    self.s_player.play()
                    Clock.schedule_once(lambda x: self.do_waiter_sound(), length)
                else:
                    target = self.rnds[self.lvl]
                    self.s_player = SoundLoader.load(selected_mod[target])
                    length = self.s_player.length
                    self.s_player.play()
                    Clock.schedule_interval(self.wait_for_jump, 0.001)
                    self.lvl += 1
                    self.rnd_sel_clck = Clock.schedule_once(lambda x: self.rand_select(), self.setting["m_m_time"])

    def do_waiter_sound(self):
        self.timer_on()
        Clock.schedule_interval(self.wait_for_jump, 0.001)

    def timer_on(self):
        self.f_vis_time = datetime.now()
        # self.ref_timer = Clock.schedule_interval(lambda x: self.timer_up(), .001)
        Clock.schedule_interval(self.timer_up, .001)

    def timer_off(self):
        # self.ref_timer.cancel()
        Clock.unschedule(self.timer_up)

    def timer_up(self, dt):
        tt = datetime.now() - self.f_vis_time
        self.vis_time = round(tt.total_seconds() * 1000)

        txt = str(self.vis_time / 1000.)

        u_t = txt.split(".")[1]

        if len(u_t) < 3:
            txt = txt + str("0") * (3 - len(u_t))
        self.parent.ids.ref_time.text = txt

    def start_listening(self):
        self.listening_start_time = datetime.now()
        self.th = Thread(target=self.wait_for_responde)
        self.th.start()

    def wait_for_responde(self):
        tim_out = 100
        while True:
            try:
                Data = self.clientsocket.recv(1024).decode()
            except ConnectionResetError:
                self.th.do_run = False
                return False
            if (datetime.now() - self.listening_start_time).total_seconds() > tim_out:
                print("time out riched")
                return False

            if Data == '1':
                self.rand_select()
                return True

    def _send(self):
        rnd_col = self.cols_list.copy()
        random.shuffle(rnd_col)
        for i in range(8):
            rnd_col[i] = self.cols_code[rnd_col[i]]

        self.ans_pos = {0: rnd_col[0], 1: rnd_col[1], 2: rnd_col[2], 3: rnd_col[3], 5: rnd_col[4], 6: rnd_col[5],
                        7: rnd_col[6], 8: rnd_col[7]}

        msg = "msg>>" + str(rnd_col)
        self.clientsocket.send(msg.encode())

    def fail(self):
        # print("******AZ Failed***")
        self.timer_off()
        self.parent.add_try(0, 0)
        self.add_try([0, 0, 0])


        # Clock.unschedule(self.rand_select)
        # self.
        self.rnd_sel_clck.cancel()
        self.clear_table()
        self.pause()
        pass

    def wait_for_jump(self, dt):

        if self.parent.btn_state[4] != 1:
            if self.setting["az_type"] == 2:
                if self.lvl != self.m_cnt - 1:
                    Clock.unschedule(self.wait_for_jump)
                    self.fail()
                    return
            self.reactin_time_set()
            Clock.unschedule(self.wait_for_jump)
            Clock.schedule_interval(self.wait_for_land, 0.001)

    def wait_for_land(self, dt):
        if self.setting["az_type"] == 1:
            for i in range(9):
                if self.parent.btn_state[i] != 0:
                    if self.check_ans(i):
                        self.answers[self.lvl] = 1
                        if all(self.answers):
                            self.movement_time_set()
                            self.ans_dt[self.lvl] = [self.react_time, self.movement_time,
                                                     self.react_time + self.movement_time]

                            avg_react = sum(v[0] for v in self.ans_dt) / (self.lvl + 1)
                            avg_mvm = sum(v[1] for v in self.ans_dt) / (self.lvl + 1)

                            self.parent.add_try(avg_react, avg_mvm)
                            self.add_try(self.ans_dt)
                            self.timer_off()
                            self.clear_table()
                            Clock.unschedule(self.wait_for_land)
                        else:
                            self.movement_time_set()
                            self.ans_dt[self.lvl] = [self.react_time, self.movement_time,
                                                     self.react_time + self.movement_time]

                            avg_react = sum(v[0] for v in self.ans_dt)/(self.lvl + 1)
                            avg_mvm = sum(v[1] for v in self.ans_dt)/(self.lvl + 1)
                            self.parent.update_try(avg_react, avg_mvm)

                            self.lvl += 1
                            Clock.unschedule(self.clear_table)
                            self.clear_table()
                            self.timer_off()
                            Clock.unschedule(self.wait_for_land)
                            Clock.schedule_once(lambda x: self.rand_select(), self.setting["m_m_time"])

        elif self.setting["az_type"] == 2:
            for i in range(9):
                if self.parent.btn_state[i] != 0:
                    if self.lvl == (self.m_cnt - 1):
                        if self.check_ans(i):
                            self.movement_time_set()
                            self.parent.add_try(self.react_time, self.movement_time)
                            self.add_try([self.react_time, self.movement_time, self.react_time + self.movement_time])

                            self.timer_off()
                            # msg = "msg>>" + "1,0,0,0,0,0,0,0,0"
                            # self.clientsocket.send(msg.encode())
                            self.clear_table()
                            try:
                                self.double_motivator.stop()
                                self.double_motivator_satate = False
                            except:
                                msg = "fun>> svmR"
                                self.clientsocket.send(msg.encode())
                                self.double_motivator_satate = False
                            Clock.unschedule(self.wait_for_land)
                            return
                        else:
                            self.fail()

        else:

            for i in range(9):
                if self.parent.btn_state[i] != 0:
                    if self.check_ans(i):
                        self.answers[self.lvl] = 1
                        if all(self.answers):
                            self.movement_time_set()
                            self.parent.add_try(self.react_time, self.movement_time)
                            self.add_try([self.react_time,self.movement_time,self.react_time+self.movement_time])
                            self.timer_off()
                            try:
                                self.double_motivator.stop()
                                self.double_motivator_satate = False
                            except:
                                msg = "fun>> svmR"
                                self.clientsocket.send(msg.encode())
                                self.double_motivator_satate = False
                            self.clear_table()
                            Clock.unschedule(self.wait_for_land)
                        else:
                            self.lvl += 1

    def add_try(self, ress: list()):
        if self.setting["az_type"] == 1:
            for i in range(len(ress)):
                self.ress = self.ress + ress[i].copy()
            if len(self.ress) == (9*self.setting["m_cnt"]):

                self.player_add_res()
                self.ress = list()

        else:
            if len(self.ress) < 6:
                self.ress = self.ress + ress

            else:
                self.ress = self.ress + ress
                self.player_add_res()
                self.ress = list()

        return

    def check_ans(self, ans):
        btn_dict = {0: 8, 1: 1, 2: 2, 3: 7, 4: 0, 5: 3, 6: 6, 7: 5, 8: 4}

        if ans == 4: return False

        if self.setting["az_type"] == 1:
            if btn_dict[ans] == self.rnds[self.lvl]:
                return True
            else:
                return False

        if btn_dict[ans] == self.T_ans:
            return True
        else:
            return False

    def pre_screen(self):
        # dt = ""
        # self.T_ans = list()
        #
        # for i in range(self.col_count):
        #     self.run_id = int((random.random() * datetime.now().microsecond) % 8)
        #
        #     while True:
        #         if self.cols_list[self.run_id] in self.T_ans:
        #             self.run_id = int((random.random() * datetime.now().microsecond) % 8)
        #         else:
        #             break
        #
        #     if self.sazegary:
        #         self.run_id_2 = self.run_id
        #
        #     else:
        #         self.run_id_2 = int((random.random() * datetime.now().microsecond) % 8)
        #         while True:
        #             if self.run_id_2 == self.run_id:
        #                 self.run_id_2 = int((random.random() * datetime.now().microsecond) % 8)
        #             else:
        #                 break
        #
        #     data = [self.cols_fa[self.cols_list[self.run_id]], self.cols_code[self.cols_list[self.run_id_2]]]
        #
        #     self.T_ans.append(self.cols_list[self.run_id])
        #
        #     dt = dt + ";" + data[0] + ":" + str(data[1])
        #
        # for i in range(3 - self.col_count):
        #     dt = dt + ";" + "" + ":" + str((0, 0, 0, 0))
        #
        # msg = "fun>> psc"
        #
        # msg = msg + dt
        # self.clientsocket.send(msg.encode())
        # time_to_wait = float(self.wait_time.text)
        # Clock.schedule_once(lambda x: self.pre_start(), time_to_wait)
        pass

    def go_test_sound(self):
        self.ready_sound.stop()
        self.go_sound.volume = 1
        self.go_sound.play()
        Clock.schedule_once(lambda x: self.go_sound.stop(), .5)
        Clock.schedule_once(lambda x: self.rand_select(), .5)

    def go_test_num_count(self, sec):
        self.start_listening()

    def go_test_colory(self, tm):
        msg= "fun>> psd2G"
        Clock.schedule_once(lambda x: self.clientsocket.send(msg.encode()), tm)
        self.start_listening()

    def pre_start(self):

        msg = "fun>> nps"
        self.clientsocket.send(msg.encode())

        w_type = self.setting["warner"]
        p_time = float(self.setting["pish_time"])
        if w_type == 0:
            self.ready_sound.play()
            Clock.schedule_once(lambda x: self.go_test_sound(),p_time)

        elif w_type == 1:
            # numering Warner
            msg = "fun>> psd1"
            sec = int(p_time)
            msg = msg + str(sec)
            self.clientsocket.send(msg.encode())
            self.go_test_num_count(sec)

        else:
            #display yelllow Green

            msg = "fun>> psd2Y"
            self.clientsocket.send(msg.encode())
            self.go_test_colory(p_time)

    def reactin_time_set(self):
        self.react_time = self.vis_time

    def movement_time_set(self):
        self.movement_time = self.vis_time - self.react_time

    def clear_table(self, dt=0):
        msg = "msg>>" + "1,0,0,0,0,0,0,0,0"
        self.clientsocket.send(msg.encode())

    def reset(self):
        self.timer_off()
        msg = "fun>> rst"
        self.clientsocket.send(msg.encode())
        # Clock.unschedule(self.wait_for_jump)
        # Clock.unschedule(self.wait_for_land)
        pass

    def enable_report(self):
        self.bx = BoxLayout(padding=20)
        self.r_btn = Button_f(text="گزارش گیری", font_size=24, pos_hint={"center_y":.5,"center_x":.5})
        self.r_btn.bind(on_release=self.report)
        self.bx.add_widget(self.r_btn)
        self.additional_setting = self.bx
        return

    def player_add_res(self):
        iid = self.parent.players[self.parent.player_num]
        dct = self.parent.sql.player_extract_ID(iid)
        name = Make_persian(dct['name'])
        # birthdate = dct['age']
        # sex = dct['sex']

        self.players_data.append([name]+self.ress.copy())

    def report(self, *args):

        if self.setting["az_type"] == 0:
            # game setting
            az_type = "ساده"
            w = {0:"بوق", 1:"شمارش", 2:"زرد-سبز"}
            m_pre = {0:"دیداری",1:"شنیداری"}
            m_t = {0:"عدد",1:"جهت",2:"رنگ"}

            q = {0:"",1:"",2:""}
            warner = w[self.setting["warner"]]
            warner_time = self.setting["pish_time"]
            m_present = m_pre[self.setting["m_present"]]
            m_type = m_t[self.setting["m_type"]]

            if self.setting["dbl_m"]:
                dbl = 'دارد'
            else:
                dbl = ''


            game_des = {"az_type":az_type,
                   "m_cnt":1,
                   "warner":warner,
                   "pish_time":warner_time,
                   "m_present":m_present,
                   "m_type":m_type,
                   "m_p_time":"----",
                   "m_m_time":"----",
                   "dbl_m":dbl,
                   "dbl_m_addr":self.setting["dbl_m_addr"],
                   "nasazegary":"ندارد"}

            # p1 = ["حسین جعفری", 1, 2, 3, 4, 5, 6, 7, 8, 9]
            # p2 = ["سونیا بی نظیر", 1, 2, 3, 4, 5, 6, 7, 8, 9]
            # p3 = ["محمد علی کیهانی", 1, 2, 3, 4, 5, 6, 7, 8, 9]
            # res = [p1, p2, p3]
            print(self.players_data)
            XLSX.put_game_des(game_des, self.players_data)

        elif self.setting["az_type"] == 1:
            az_type = "انتخابی"
            w = {0: "بوق", 1: "شمارش", 2: "زرد-سبز"}
            m_pre = {0: "دیداری", 1: "شنیداری"}
            m_t = {0: "عدد", 1: "جهت", 2: "رنگ"}

            warner = w[self.setting["warner"]]
            warner_time = self.setting["pish_time"]
            m_present = m_pre[self.setting["m_present"]]
            m_type = m_t[self.setting["m_type"]]

            if self.setting["dbl_m"]:
                dbl = 'دارد'
            else:
                dbl = ''

            game_des = {"az_type": az_type,
                        "m_cnt": self.setting["m_cnt"],
                        "warner": warner,
                        "pish_time": warner_time,
                        "m_present": m_present,
                        "m_type": m_type,
                        "m_p_time": self.setting["m_p_time"],
                        "m_m_time": self.setting["m_m_time"],
                        "dbl_m": dbl,
                        "dbl_m_addr": self.setting["dbl_m_addr"],
                        "nasazegary": self.setting["nasazegary"]}

            # p1 = ["حسین جعفری", 1, 2, 3, 4, 5, 6, 7, 8, 9]
            # p2 = ["سونیا بی نظیر", 1, 2, 3, 4, 5, 6, 7, 8, 9]
            # p3 = ["محمد علی کیهانی", 1, 2, 3, 4, 5, 6, 7, 8, 9]
            # res = [p1, p2, p3]
            print(self.players_data)
            XLSX.put_game_des(game_des, self.players_data)

            pass

        elif self.setting["az_type"] == 2:
            # game setting
            az_type = "افتراقی"
            w = {0: "بوق", 1: "شمارش", 2: "زرد-سبز"}
            m_pre = {0: "دیداری", 1: "شنیداری"}
            m_t = {0: "عدد", 1: "جهت", 2: "رنگ"}

            warner = w[self.setting["warner"]]
            warner_time = self.setting["pish_time"]
            m_present = m_pre[self.setting["m_present"]]
            m_type = m_t[self.setting["m_type"]]

            if self.setting["dbl_m"]:
                dbl = 'دارد'
            else:
                dbl = ''

            game_des = {"az_type": az_type,
                        "m_cnt": self.setting["m_cnt"],
                        "warner": warner,
                        "pish_time": warner_time,
                        "m_present": m_present,
                        "m_type": m_type,
                        "m_p_time": self.setting["m_p_time"],
                        "m_m_time": self.setting["m_m_time"],
                        "dbl_m": dbl,
                        "dbl_m_addr": self.setting["dbl_m_addr"],
                        "nasazegary": self.setting["nasazegary"]}

            # p1 = ["حسین جعفری", 1, 2, 3, 4, 5, 6, 7, 8, 9]
            # p2 = ["سونیا بی نظیر", 1, 2, 3, 4, 5, 6, 7, 8, 9]
            # p3 = ["محمد علی کیهانی", 1, 2, 3, 4, 5, 6, 7, 8, 9]
            # res = [p1, p2, p3]
            print(self.players_data)
            XLSX.put_game_des(game_des, self.players_data)

        print("Report")



