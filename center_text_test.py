# -*- coding: utf-8 -*-
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.properties import StringProperty, NumericProperty, BooleanProperty
from bidi.algorithm import get_display
import arabic_reshaper
from tkinter import *


def Make_persian(string):
    string = get_display(arabic_reshaper.reshape(string))
    return string


Builder.load_string('''
<Text_fa>:
    font_name: 'data/Tahoma Regular font.ttf'

<Test>:
    Text_fa:
        text: 't'
        font_size: 60
        # left, right
        padding_x:
            [self.center[0] - self._get_text_width(max(self._lines, key=len), self.tab_width, self._label_cached) / 2.0,
            0] if self.text else [self.center[0], 0]
        # top, bottom
        padding_y: [self.height / 2.0 - (self.line_height / 2.0) * len(self._lines), 0]
''')
class Text_fa(TextInput):
    max_chars = NumericProperty(1000)
    acceptable_chars = u'ابپتثجچح  خدذرزسشصضطظعغفقکگلمنوهیabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ[];,.":۱۲۳۴۵۶۷۸۹۰\n'
    str = StringProperty()

    def insert_text(self, substring, from_undo=False):
        if not (substring in self.acceptable_chars):
            return
        if not from_undo and (len(self.text) + len(substring) > self.max_chars):
            return
        self.str = self.str + substring
        self.text = Make_persian(self.str)
        substring = ""
        super(Text_fa, self).insert_text(substring, from_undo)

    def do_backspace(self, from_undo=False, mode='bkspc'):
        self.str = self.str[0:len(self.str) - 1]
        self.text = Make_persian(self.str)

    pass
class Test(BoxLayout):
    pass
class TesttextApp(App):
    def build(self):
        return Test()
TesttextApp().run()