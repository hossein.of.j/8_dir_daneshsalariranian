import sys
import serial
import time

start_cmd = bytearray([250,112,17,85])
stop_cmd = bytearray([250,112,18,int(0x66)])

class SER:

    def find_ser_port(self):

        f_port = None
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        for port in result:
            s = serial.Serial(port, timeout=.01)
            s.write(start_cmd)
            res = s.read(4)
            target = bytearray([int(0xeb), int(0x90), int(0x11), int(0x55)])
            s.write(stop_cmd)
            s.close()
            if res == target:
                f_port = port
        return f_port

    def serial_initialize(self):
        cnt = 0
        while cnt <= 10:
            cnt+=1
            port = self.find_ser_port()
            if port:
                self.ser = serial.Serial(port)
                if not self.ser.is_open:
                    self.ser.open()
                self.ser.write(start_cmd)
                res = self.ser.read(4)
                if res == bytearray([int(0xeb), int(0x90), int(0x11), int(0x55)]):
                    return True
                    break
            else:
                pass
        return False

    def close_serial(self):
        try:
    	    self.ser.write(stop_cmd)
    	    self.ser.close()
        except:
            pass

    def read_state(self):
        self.ser.flushInput()
        res = self.ser.read(4)
        b8 = bin(res[1])[-1]
        b7_0 = bin(res[2])[2:]
        if len(b7_0) < 8:
            b7_0 = "0" * (8 - len(b7_0)) + b7_0
        bits = b8 + b7_0
        btns = [int(bits[8-i]) for i in range(9)]
        return btns


if __name__ == "__main__":
    ser = SER()
    if ser.serial_initialize():
        print("serial started")
        for i in range(128):
            ser.read_state()
            time.sleep(.2)
        ser.close_serial()

    pass