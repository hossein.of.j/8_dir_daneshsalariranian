import sys
import glob
import serial
import time
import datetime

def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

start_cmd = bytearray([250,112,17,85])

if __name__ == '__main__':

    a = bytearray([int(0xfa), int(0x70), int(0x50), int(0x55)])
    b = bin(a[2])
    print(b)
    b= b[2:]
    print(b)
    if len(b)<8:
        b="0"*(8-len(b)) + b
    print(b)
    print(b[0],b[1],b[2])

    # ser = serial.Serial(port='COM13', timeout=.1)
    # # ser.write(start_cmd)
    # start = datetime.datetime.now()
    # print(start.minute)
    # dt = bytes()
    # print("start at ", start)
    #
    # while (datetime.datetime.now().microsecond < start.microsecond) or (datetime.datetime.now().second < start.second) or (datetime.datetime.now().minute <=start.minute):
    #     dt = dt + ser.read_all()
    #     pass
    #
    # stop = datetime.datetime.now()
    # print("stop  at ", stop)
    # print(len(dt))
    # print(dt[0])
    # f= open("input.txt",'w')
    # f.write(str(dt))
    # f.close()
    # #
    # #     h = ser.read(10)
    # #     if h:
    # #         print(h)
