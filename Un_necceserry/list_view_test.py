from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.lang.builder import Builder
from kivy.uix.listview import SelectableView, ListItemReprMixin
from kivy.adapters.models import SelectableDataItem
from kivy.adapters.listadapter import ListAdapter
from kivy.uix.button import Button
from kivy.uix.screenmanager import Screen
from kivy.properties import StringProperty



data = [
    {'name': 'francis', 'age': '18', 'group': 'Fencing', 'is_selected': False},
    {'name': 'sami talebian', 'age': '23', 'group': 'html', 'is_selected': False},
    {'name': 'ali bahreini', 'age': '32', 'group': 'basket', 'is_selected': False},
    {'name': 'daniel ', 'age': '14', 'group': 'volleyball', 'is_selected': False},
    {'name': 'charter', 'age': '22', 'group': 'Fencing', 'is_selected': False},
    {'name': 'jakie', 'age': '15', 'group': 'Football', 'is_selected': False}
]






Builder.load_string('''
<CILabel>:
<CIButton>:
    background_normal: ''

<CIBoxLayout>
<CItem>:

    orientation: "horizontal"
    size_hint_y: None
    height: "60dp"
    selected_color: (0.9,0.7,0.7,1)
    deselected_color: (1,1,1,1)

    CIButton:
        text: root.name
        color: 0.5,0.1,.5,1
        size_hint_x: 0.4
        background_color: root.selected_color if root.is_selected else root.deselected_color
    CIButton:
        text: root.age
        color: 0.5,0.1,.5,1
        size_hint_x: 0.3
        background_color: root.selected_color if root.is_selected else root.deselected_color
    CIButton:
        text: root.group
        color: 0.5,0.1,.5,1
        size_hint_x: 0.3
        background_color: root.selected_color if root.is_selected else root.deselected_color
    
                
<CatalogScreen>:
    BoxLayout:
        size_hint: (1.0, 1.0)
        ListView:
            size_hint: (1.0, 1.0)
            id: Listing
        Button:
            text: "finish"
            on_release: root.doer()
        
''')

class CIButton(ListItemReprMixin, SelectableView, Button):

    def select(self, *args):
        if isinstance(self.parent, CItem) or isinstance(self.parent, CIBoxLayout):
            self.parent.select_from_child(self, *args)

    def deselect(self, *args):
        if isinstance(self.parent, CItem) or isinstance(self.parent, CIBoxLayout):
            self.parent.deselect_from_child(self, *args)



class CIBoxLayout(SelectableView, BoxLayout):

    def select(self, *args):
        if isinstance(self.parent, CItem):
            self.parent.select_from_child(self, *args)

    def deselect(self, *args):
        if isinstance(self.parent, CItem):
            self.parent.deselect_from_child(self, *args)

    def select_from_child(self, child, *args):
        self.select()

    def deselect_from_child(self, child, *args):
        self.deselect()


class DeeperListAdapter(ListAdapter):

    def create_view(self, index):
        '''Extends ListAdapter to bind events from grandchild and great
        grandchild and allow more elaborate combinations of widgets.
        '''
        item = self.get_data_item(index)
        print "item >> ", item
        if item is None:
            return None

        item_args = self.args_converter(index, item)
        # print "item_args >>", item_args
        item_args['index'] = index

        cls = self.get_cls()
        print "cls >> ", cls
        if cls:
            view_instance = cls(**item_args)

        else:
            print "in out"
            view_instance = Builder.template(self.template, **item_args)

        if self.propagate_selection_to_data:
            # The data item must be a subclass of SelectableDataItem, or must
            # have an is_selected boolean or function, so it has is_selected
            # available. If is_selected is unavailable on the data item, an
            # exception is raised.
            #
            if isinstance(item, SelectableDataItem):
                if item.is_selected:
                    self.handle_selection(view_instance)
            elif type(item) == dict and 'is_selected' in item:
                if item['is_selected']:
                    self.handle_selection(view_instance)
            elif hasattr(item, 'is_selected'):
                if (inspect.isfunction(item.is_selected)
                        or inspect.ismethod(item.is_selected)):
                    if item.is_selected():
                        self.handle_selection(view_instance)
                else:
                    if item.is_selected:
                        self.handle_selection(view_instance)
            else:
                msg = "ListAdapter: unselectable data item for {0}"
                raise Exception(msg.format(index))

        view_instance.bind(on_release=self.handle_selection)

        # the only actual change to this method is to bind 2 levels deeper

        for child in view_instance.children:
            child.bind(on_release=self.handle_selection)
            for gchild in child.children:
                gchild.bind(on_release=self.handle_selection)
                for ggchild in gchild.children:
                    ggchild.bind(on_release=self.handle_selection)

        return view_instance


class CItem(SelectableView, BoxLayout):
    name= StringProperty()
    age = StringProperty()
    group = StringProperty()
    def select(self, *args):
        self.is_selected = True

    def deselect(self, *args):
        self.is_selected = False

    def select_from_child(self, child, *args):
        self.select()

    def deselect_from_child(self, child, *args):
        self.deselect()



class CatalogScreen(Screen):
    def __init__(self, *args, **kwargs):
        self.klist = data
        # print "klist >>> ", self.klist
        # self.ac = lambda row_index, an_obj: {'name': an_obj['name'],
        #                                      'age': an_obj['age'],
        #                                      'group': an_obj['group']}

        self.catalogLA = ListAdapter(data=self.klist, args_converter=self.args_converter,
                                           cls=CItem, selection_mode='multiple',
                                           allow_empty_selection=True, propagate_selection_to_data=True)


        args_converter = lambda row_index, obj: {'name': obj['name']}
        #                                          'age': }
        # self.catalogLA= DeeperListAdapter(data=self.klist, args_converter=self.args_converter,
        #                             cls=CItem,
        #                             selection_mode='single')

        self.catalogLA.bind(on_selection_change=self.update_selection)
        super(self.__class__, self).__init__(*args, **kwargs)
        self.ids.Listing.adapter = self.catalogLA


    def update_selection(self, adapter):
        print "selection updated=", self.catalogLA.selection

        # print fruit

    def args_converter(self, row_index, an_obj):

        return {'name': an_obj['name'],
                'age': an_obj['age'],
                'group': an_obj['group']}

    def doer(self):
        self.catalogLA.data= sorted(self.klist, key=lambda item: item['group'])#== 'Fencing')
        for i in range(self.catalogLA.get_count()):
            print i, " >> ", self.catalogLA.get_view(i).name, " >> ", self.catalogLA.get_view(i).is_selected
        print "finish"

    def sorter(self, item):
        print item
        if item['group']== 'Fencing':
            return 0
        else:
            return 10

class listtestapp(App):
    def build(self):
        return CatalogScreen()






samplapp=listtestapp()
samplapp.run()