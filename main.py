# -*- coding: utf-8 -*-
ayat_al_corsi = '''بسم الله الرحمن الرحیم
Allah—there is no god except Him—is the Living One, the All-sustainer. Neither drowsiness befalls Him nor sleep. To Him belongs whatever is in the heavens and whatever is on the earth. Who is it that may intercede with Him except with His permission?
He knows what is before them and what is behind them, and they do not comprehend anything of His knowledge except what He wishes. His seat embraces the heavens and the earth and He is not wearied by their preservation, and He is the All-exalted, the All-supreme. (255)
There is no compulsion in religion: rectitude has become distinct from error. So one who disavows fake deities and has faith in Allah has held fast to the firmest handle for which there is no breaking; and Allah is all-hearing, all-knowing. (256)
Allah is the wali of the faithful: He brings them out of darkness into light. As for the faithless, their awliya are the fake deities, who drive them out of light into darkness. They shall be the inmates of the Fire, and they will remain in it [forever]. (257)
<<Baghare>>
'''
print(ayat_al_corsi)

from kivy.app import App
from kivy.uix.relativelayout import RelativeLayout
from kivy.core.window import Window
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.textinput import TextInput
from kivy.graphics import Rectangle, Color

from screens_classes import *
import database_manipulator as dt_base
import subprocess

from kivy.config import Config
import win32gui, win32con


#Config.set('kivy', 'exit_on_escape', '0')

Window.maximize()
# Window.left = 0
# Window.size = (1100, 900)
# Window.top = 20
Window.clearcolor = (229.0 / 256.0, 239.0 / 256.0, 246.0 / 256.0, 1)


def Make_persian(string):
    string = get_display(arabic_reshaper.reshape(string))
    return string


def Name_putter(Master):

    Master.ids.sc_manager.ids.documenter.ids.doc_list_crtd_date.text = Make_persian("تاریخ تشکیل پرونده")
    Master.ids.sc_manager.ids.documenter.ids.doc_list_group.text = Make_persian("گروه")
    Master.ids.sc_manager.ids.documenter.ids.doc_list_sex.text = Make_persian("جنسیت")
    Master.ids.sc_manager.ids.documenter.ids.doc_list_age.text = Make_persian("تاریخ تولد")
    Master.ids.sc_manager.ids.documenter.ids.doc_list_name.text = Make_persian("نام ونام خانوادگی")

    Master.ids.sc_manager.ids.extractor.ids.doc_list_crtd_date.text = Make_persian("تاریخ تشکیل پرونده")
    Master.ids.sc_manager.ids.extractor.ids.doc_list_group.text = Make_persian("گروه")
    Master.ids.sc_manager.ids.extractor.ids.doc_list_sex.text = Make_persian("جنسیت")
    Master.ids.sc_manager.ids.extractor.ids.doc_list_age.text = Make_persian("تاریخ تولد")
    Master.ids.sc_manager.ids.extractor.ids.doc_list_name.text = Make_persian("نام ونام خانوادگی")


class Fa_text(TextInput):
    max_chars = NumericProperty(20)
    str = StringProperty()
    acceptable_chars = u'ابپتثجچح  خدذرزسشصضطظعغفقکگلمنوهی۱۲۳۴۵۶۷۸۹۰'
    mod = "fa"

    def insert_text(self, substring, from_undo=False):
        if self.mod == 'fa':
            if not (substring in self.acceptable_chars):
                return

        if not from_undo and (len(self.text) + len(substring) > self.max_chars):
            return
        self.str = self.str + substring
        self.text = Make_persian(self.str)
        substring = ""
        super(Fa_text, self).insert_text(substring, from_undo)

    def do_backspace(self, from_undo=False, mode='bkspc'):
        # super(Fa_text, self).do_backspace()
        self.str = self.str[0:len(self.str) - 1]
        self.text = Make_persian(self.str)

    def delete_selection(self):
        super(Fa_text, self).delete_selection()
        self.str = Make_persian(self.text)
        self.text = Make_persian(self.str)


class Fa_text_2(TextInput):
    max_chars = NumericProperty(20)
    acceptable_chars = u'ابپتثجچح  خدذرزسشصضطظعغفقکگلمنوهیabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ[];,.":۱۲۳۴۵۶۷۸۹۰\n'
    str = StringProperty()

    def insert_text(self, substring, from_undo=False):
        if not (substring in self.acceptable_chars):
            return
        if not from_undo and (len(self.text) + len(substring) > self.max_chars):
            return
        self.str = self.str + substring
        self.text = Make_persian(self.str)
        substring = ""
        super(Fa_text_2, self).insert_text(substring, from_undo)

    def do_backspace(self, from_undo=False, mode='bkspc'):
        self.str = self.str[0:len(self.str) - 1]
        self.text = Make_persian(self.str)

    def delete_selection(self):
        super(Fa_text_2, self).delete_selection()
        self.str = Make_persian(self.text)
        self.text = Make_persian(self.str)


class Menutitle(ButtonBehavior, BoxLayout):
    percentage = NumericProperty(100)

    def __init__(self, **kwargs):
        super(Menutitle, self).__init__(**kwargs)
        self.bind(on_press=lambda x: self.openMenu())
        self.rec = Rectangle()
        self.bind(size=self.rec_up,
                  pos=self.rec_up)

    def rec_up(self, *args):
        self.rec.size= self.size
        self.rec.pos = self.pos

    def openMenu(self):
        for child in self.parent.children:

            if (child.menid != self.menid) & (child.en):
                child.Make_off()

        self.Make_on()
        pass

    def Make_off(self):
        self.ids.txt.color = (0, 0, 0, 1)
        self.en = False

        for child in self.parent.children:
            if child.menid.split('_')[0] == 'cp':

                if child.menid.split('_')[1] == self.menid.split('_')[1]:

                    child.text_col = (0, 0, 0, 0)
                    child.size_hint = (0, 0)


        self.canvas.before.clear()

    def Make_on(self):
        self.ids.txt.color = (0, 0.2, 0.6, 1)

        self.en = True

        for child in self.parent.children:
            if child.menid.split('_')[0] == 'cp':

                if child.menid.split('_')[1] == self.menid.split('_')[1]:
                    child.text_col = (0, 0, 0, 1)
                    child.size_hint = (1, None)
                    child.size = (350, 40)
        Clock.schedule_once(self.bg_col, 0)

    def bg_col(self, dt):
        with self.canvas.before:
            Color(135/256., 206/256., 235/256.)
            self.rec = Rectangle(pos=self.pos, size=(self.size[0] * (self.percentage / 100.0), self.size[1]))


class C_Menutitle(ButtonBehavior, BoxLayout):
    percentage = NumericProperty(100)

    def __init__(self, **kwargs):
        super(C_Menutitle, self).__init__(**kwargs)
        self.bind(on_press=lambda x: self.openMenu())

    def openMenu(self):
        for child in self.parent.children:
            # (child.menid.split('_')[0] == "cp")
            if  ("cp" in child.menid)& (child.menid != self.menid) & (child.en):
                child.Make_off()
        self.Make_on()


    def Make_off(self):
        self.ids.txt.color = (0, 0, 0, 0)
        self.en = False
        self.canvas.before.clear()

    def Make_on(self):
        self.ids.txt.color = (0, 0.2, 0.6, 1)

        self.en = True
        Clock.schedule_once(self.bg_col, -1)

    def bg_col(self, dt):
        with self.canvas.before:
            Color(135/256., 206/256., 235/256.)
            Rectangle(pos=self.pos, size=(self.size[0] * (self.percentage / 100.0), self.size[1]))


class Main_screen(BoxLayout):

    editor_id = None
    player_list = []
    online_infs = ['', '', '', '']

    def __init__(self, **kwargs):


        super(Main_screen, self).__init__(**kwargs)

        bxt = PassBox()
        pop = Popup(title=Make_persian(''), content=bxt, size_hint=(None, None), size=(400, 230), auto_dismiss=False)

        # Clock.schedule_once(lambda x: pop.open())

        Name_putter(self)

        self.ids.sc_manager.current = "Splash"
        # self.ids.sc_manager.current = "Testmode"
        self.ids.Dt_bar_Per_name.text = Make_persian(self.online_infs[0])
        self.ids.Dt_bar_group_name.text = Make_persian(self.online_infs[1])
        self.ids.Dt_bar_Data_2.text = Make_persian(self.online_infs[2])
        self.ids.Dt_bar_Data_1.text = Make_persian(self.online_infs[3])

    def online_inf_update(self):
        self.ids.Dt_bar_Per_name.text = Make_persian(self.online_infs[0])
        self.ids.Dt_bar_group_name.text = Make_persian(self.online_infs[1])
        self.ids.Dt_bar_Data_2.text = Make_persian(self.online_infs[2])
        self.ids.Dt_bar_Data_1.text = Make_persian(self.online_infs[3])


class PassBox(RelativeLayout):
    global Base

    def check_login(self, user, paswd, master):
        sql_mngr = dt_base.data_base()
        res = sql_mngr.login_check(user, paswd)

        if res == "UserNameError":
            Popup(title="error", content=Label(text="Wrong Username"), size_hint=(0.3, 0.3)).open()

        elif res == "PasswdError":
            Popup(title="error", content=Label(text="Wrong Password"), size_hint=(0.3, 0.3)).open()
        else:
            master.dismiss()
            Base.Base.editor_id = user


class eightdirApp(App):
    Base = None

    def build(self):
        self.Base = Main_screen()
        return self.Base

    def on_stop(self):
        hndle = win32gui.FindWindow(None, "Second")
        win32gui.PostMessage(hndle, win32con.WM_CLOSE, 0, 0)
        pass


#if __name__ == "__main__":

Base = eightdirApp()
Base.run()
